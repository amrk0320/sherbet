//
//  top3ViewController.swift
//  play
//
//  Created by munetomoissei on 2016/06/02.
//  Copyright © 2016年 munetomoissei. All rights reserved.
//

import UIKit
import Firebase
import AVFoundation

class Top3ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,AVAudioPlayerDelegate{
    
    // セルのインスタンスを作成(高さ取得メソッドを使う為)
    var heightLeftCell: CustomLeftTableViewCell = CustomLeftTableViewCell()
    var heightRightCell: CustomRightTableViewCell = CustomRightTableViewCell()
    
    // firebaseセットアップ
    var ref: Firebase = Firebase(url: "https://playchatting.firebaseio.com/")
    var refRoom: Firebase = Firebase(url: "https://playroom.firebaseio.com/")
    // username
    var name:String = ""
    // AppDelegateクラスのメンバー変数を参照する
    let app:AppDelegate =
        (UIApplication.sharedApplication().delegate as! AppDelegate)
    // room名
    var roomName:String!
    // refreshCount
    var refreshCount:Int = 0
    
    var AudioPlayer = AVAudioPlayer ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(red:30.0/255,green:30.0/255,blue:30.0/255,alpha:1.0)
        
        // データを初期化します
        app.top3tableData = []
        
        //前回の保存内容があるかどうかを判定
        if((NSUserDefaults.standardUserDefaults().objectForKey("top3")) != nil){
            
            //objectsを配列として確定させ、前回の保存内容を格納
            let objects = NSUserDefaults.standardUserDefaults().objectForKey("top3") as? NSArray
            
            //各名前を格納するための変数を宣言
            var _:AnyObject
            
            //前回の保存内容が格納された配列の中身を一つずつ取り出す
            for nameString in objects!{
                //配列に追加していく
                app.top3tableData.append(nameString as! Dictionary<String,AnyObject>)
            }
        }
        
        // usernameの取得
        self.name = app.name!
        
        //self.title = app.topRoomName[0]
        
        // 最新のデータをデータベースから取得する
        // 最新のデータ追加されるたびに最新データを取得する
        ref.queryOrderedByChild("room").queryEqualToValue(app.topRoom[2]).queryLimitedToLast(1).observeEventType(FEventType.ChildAdded, withBlock: { (snapshot) in
            
            let created = snapshot.value["created"] as? String
            let text = snapshot.value["text"] as? String
            let name = snapshot.value["name"] as? String
            let senderId = snapshot.value["senderId"] as? String
            let room = snapshot.value["room"] as? String
            let bColor = snapshot.value["bColor"] as? String
            let tColor = snapshot.value["tColor"] as? String
            let fontSize = snapshot.value["fontSize"] as? String
            let backGround = snapshot.value["backGround"] as? String
            let font = snapshot.value["font"] as? String
            
            // 初めてのトークでなければ
            if self.app.top3tableData.count != 0 {
                
                if created != self.app.top3tableData[self.app.top3tableData.count-1]["created"] as? String {
                    
                    
                    let dictionary:Dictionary<String,AnyObject> = ["created": created!, "name": name!,"senderId": senderId!, "text":text!,"room":room!,"bColor":bColor!,"tColor":tColor!,"font":font!,"fontSize":fontSize!,"backGround":backGround!]
                    self.app.top3tableData.append(dictionary)
                    
                    NSUserDefaults.standardUserDefaults().setObject(self.app.top3tableData, forKey: "top3")
                    
                    let defaults = NSUserDefaults.standardUserDefaults()
                    
                    // シンクロを入れないとうまく動作しないときがあります
                    defaults.synchronize()
                }
                
            } else {
                // 初めてのトークならば
                let array:[Dictionary<String,AnyObject>] = [["created": created!, "name": name!,"senderId": senderId!, "text":text!,"room":room!,"bColor":bColor!,"tColor":tColor!,"font":font!,"fontSize":fontSize!,"backGround":backGround!]]
                self.app.top3tableData = array
                NSUserDefaults.standardUserDefaults().setObject(array, forKey: "top3")
                NSUserDefaults.standardUserDefaults().synchronize();
            }
            
            // TableViewを再読み込み.
            self.app.top3tableView.reloadData()
            if(self.app.top3tableData.count > 4){
                self.app.top3tableView.setContentOffset(CGPointMake(0, self.app.top3tableView.contentSize.height - self.app.top3tableView.frame.size.height), animated: true)
            }
            
        })
        
        //permissionの設定.
        let settings = UIUserNotificationSettings(forTypes: UIUserNotificationType.Badge, categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        
        //バッジの数の設定.
        UIApplication.sharedApplication().applicationIconBadgeNumber = 1
        
        // サウンドを設定する
        let soundFilePath = NSURL(fileURLWithPath:NSBundle.mainBundle().pathForResource("sound/send", ofType: "mp3")!)
        AudioPlayer = try! AVAudioPlayer(contentsOfURL: soundFilePath);
        AudioPlayer.prepareToPlay()
        
        // VIEWをセットします
        setView()
        
        self.app.top3tableView.reloadData()
        self.app.top3tableView.setContentOffset(CGPointMake(0, self.app.top3tableView.contentSize.height - self.app.top3tableView.frame.size.height), animated: true)
    }
    
    
    // VIEWをセットします
    func setView()
    {
        let displayWidth = self.view.frame.width
        let displayHeight = self.view.frame.height
        self.app.top3tableView = UITableView(frame: CGRect(x:0, y:0, width:displayWidth, height:displayHeight-230))
        
        // カスタムセルクラスを呼び出し名前をつける テーブルに付与
        self.app.top3tableView.registerClass(CustomLeftTableViewCell.self, forCellReuseIdentifier: "CustomLeftTableViewCell")
        self.app.top3tableView.registerClass(CustomLeftTableViewCell.self, forCellReuseIdentifier: "CustomRightTableViewCell")
        self.app.top3tableView.delegate = self
        self.app.top3tableView.dataSource = self
        // セル間の区切り色を指定
        self.app.top3tableView.separatorColor = UIColor.clearColor()
        self.view.addSubview(self.app.top3tableView)
        
    }
    
    // テーブルセルの高さをかえします　deletegateの設定 ここが最初に全部呼ばれる
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if self.app.top3tableData.count == 0 {
            return 44
        }
        
        if self.name == self.app.top3tableData[indexPath.row]["name"] as! String
        {
            return heightRightCell.setData(tableView.frame.size.width - 20, data: self.app.top3tableData[indexPath.row])
        }
        else
        {
            return heightLeftCell.setData(tableView.frame.size.width - 20, data: self.app.top3tableData[indexPath.row])
        }
    }
    
    //セルを表示させる
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if self.app.top3tableData.count == 0 {
            // 初期化したセルを呼び出す
            let cell: CustomLeftTableViewCell = CustomLeftTableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "CustomLeftTableViewCell")
            self.setGesture(cell)
            return cell
        }
        
        if self.name == self.app.top3tableData[indexPath.row]["name"] as! String
        {
            
            // 初期化したセルを呼び出す
            let cell: CustomLeftTableViewCell = CustomLeftTableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "CustomLeftTableViewCell")
            cell.setData(self.app.top3tableView.frame.size.width - 20, data: self.app.top3tableData[indexPath.row])
            print(self.app.top3tableData[0])
            self.setGesture(cell)
            
            if indexPath.row == self.app.top3tableData.count-1 {
                if NSUserDefaults.standardUserDefaults().objectForKey("bubbleColor") != nil {
                    
                    let color = NSUserDefaults.standardUserDefaults().objectForKey("bubbleColor") as? String
                    let rFromIdx = color!.startIndex.advancedBy(0)
                    let rToIdx = color!.startIndex.advancedBy(2)
                    let gFromIdx = color!.startIndex.advancedBy(3)
                    let gToIdx = color!.startIndex.advancedBy(5)
                    let bFromIdx = color!.startIndex.advancedBy(6)
                    let bToIdx = color!.startIndex.advancedBy(8)
                    
                    let ri = Int((color?.substringWithRange(rFromIdx...rToIdx))!)
                    let r = CGFloat(ri!)
                    
                    let gi = Int((color?.substringWithRange(gFromIdx...gToIdx))!)
                    let g = CGFloat(gi!)
                    
                    let bi = Int((color?.substringWithRange(bFromIdx...bToIdx))!)
                    let b = CGFloat(bi!)
                    
                    //                    cell.viewMessage.backgroundColor = UIColor(red:r/255,green:g/255,blue:b/255,alpha:1.0)
                    //                    cell.message.backgroundColor = UIColor(red:r/255,green:g/255,blue:b/255,alpha:1.0)
                    cell.viewMessage.backgroundColor = UIColor.clearColor()
                    cell.message.backgroundColor = UIColor.clearColor()
                    
                    
                }
            }
            
            return cell
        }
        else
        {
            // 初期化したセルを呼び出す
            let cell: CustomLeftTableViewCell = CustomLeftTableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "CustomLeftTableViewCell")
            cell.setData(self.app.top3tableView.frame.size.width - 20, data: self.app.top3tableData[indexPath.row])
            self.setGesture(cell)
            return cell
        }
        
    }
    
    // セルの行数　dataSourceの設定
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.app.top3tableData.count == 0 {
            return 1
        }
        return self.app.top3tableData.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell,
                   forRowAtIndexPath indexPath: NSIndexPath) {
        self.app.top3tableView.backgroundColor = self.app.blackColor
        
        if self.app.top3tableData.count > 0 {
            
            cell.backgroundColor = self.app.blackColor
            
            
        } else {
            
            cell.backgroundColor = self.app.blackColor
        }
    }
    
    // ジェスチャーをセットさせる
    func setGesture(cell: UITableViewCell) {
        
        // タップイベントを追加させる
        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.onDoubleTap(_:)))
        doubleTapGesture.numberOfTapsRequired = 2
        cell.userInteractionEnabled = true
        cell.addGestureRecognizer(doubleTapGesture)
        
        // viewにロングタップのジェスチャーを追加
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.longPress(_:)))
        cell.addGestureRecognizer(longPressGesture)
        
    }
    
    // ダブルタップされたら
    func onDoubleTap(sender: UITapGestureRecognizer) {
        
        let text = NSUserDefaults.standardUserDefaults().objectForKey("shortCutDTap") as! String
        self.app.dTapPlayer.play()
        self.addText(text)
        
    }
    
    // 長押しされたら
    func longPress(sender: UITapGestureRecognizer) {
        
        if sender.state == .Ended{
            let text = NSUserDefaults.standardUserDefaults().objectForKey("longPress") as! String
            self.app.longPressPlayer.play()
            self.addText(text)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        // 初期表示で１番下までスクロールさせる
        
    }
    
    
    
    // セルに新しいデータを追加
    func addText(text :String) {
        
        let now = NSDate()
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        let string = formatter.stringFromDate(now)
        var bColor = "040040040"
        var tColor = "000173255"
        var font = "Verdana"
        var fontSize = "48"
        var backGround = "040040040"
        
        if NSUserDefaults.standardUserDefaults().objectForKey("bubbleColor") != nil {
            bColor = NSUserDefaults.standardUserDefaults().objectForKey("bubbleColor") as! String
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("tintColor") != nil {
            tColor = NSUserDefaults.standardUserDefaults().objectForKey("tintColor") as! String
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("font") != nil {
            font = NSUserDefaults.standardUserDefaults().objectForKey("font") as! String
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("fontSize") != nil {
            fontSize = NSUserDefaults.standardUserDefaults().objectForKey("fontSize") as! String
        }
        
        if NSUserDefaults.standardUserDefaults().objectForKey("backGround") != nil {
            backGround = NSUserDefaults.standardUserDefaults().objectForKey("backGround") as! String
        }
        
        
        //firebaseにデータを送信、保存する
        let dictionary = ["created": string, "name": self.app.name!,"senderId": self.app.userId!, "text":text,"room":app.topRoom[2],"bColor":bColor,"tColor":tColor,"font":font,"fontSize":fontSize,"backGround":backGround]
        
        let array:[Dictionary<String,AnyObject>] = [["created": string, "name": self.app.name!,"senderId": self.app.userId!, "text":text,"room":app.topRoom[2],"bColor":bColor,"tColor":tColor,"font":font,"fontSize":fontSize,"backGround":backGround]]
        
        let post1Ref = ref.childByAutoId()
        post1Ref.setValue(dictionary)
        
        // 端末にトーク履歴を保存する
        if NSUserDefaults.standardUserDefaults().objectForKey("top3") != nil {
            
            app.top3tableData.append(dictionary)
            
            NSUserDefaults.standardUserDefaults().setObject(app.top3tableData, forKey: "top3")
            
            let defaults = NSUserDefaults.standardUserDefaults()
            
            // シンクロを入れないとうまく動作しないときがあります
            defaults.synchronize()
            
            
        } else{
            app.top3tableData.append(dictionary)
            NSUserDefaults.standardUserDefaults().setObject(array, forKey: "top3")
            NSUserDefaults.standardUserDefaults().synchronize();
        }
        
        // TableViewを再読み込み.
        self.app.top3tableView.reloadData()
        if(self.app.top3tableData.count > 4){
            self.app.top3tableView.setContentOffset(CGPointMake(0, self.app.top3tableView.contentSize.height - self.app.top3tableView.frame.size.height), animated: true)
        }
        
    }
    
    // テーブルを再読み込み
    func reload() {
        //        self.app.top3tableView.reloadData()
        //    if(self.app.top3tableData.count > 4){
        //        self.app.top3tableView.setContentOffset(CGPointMake(0, self.app.top3tableView.contentSize.height - self.app.top3tableView.frame.size.height), animated: true)
        //    }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
