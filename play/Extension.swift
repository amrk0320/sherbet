//
//  Extension.swift
//  play
//
//  Created by munetomoissei on 2016/05/05.
//  Copyright © 2016年 munetomoissei. All rights reserved.
//
import Foundation
import UIKit

// UIの下に枠線を追加
extension CALayer {
    
    // 下だけBorder
    func drawUnderBorder(borderWidth: CGFloat, borderColor: UIColor, ui:AnyObject) -> CALayer{
        let border = CALayer()
        border.frame = CGRectMake(0.0, ui.frame.size.height-borderWidth, ui.frame.size.width, borderWidth)
        border.backgroundColor = borderColor.CGColor
        return border
    }
}