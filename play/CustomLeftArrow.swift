//
//  CustomLeftArrow.swift
//  play
//
//  Created by munetomoissei on 2016/05/18.
//  Copyright © 2016年 munetomoissei. All rights reserved.
//

import Foundation
import UIKit

// 吹き出しの突起部分

class CustomLeftArrow: UIView
{
    // AppDelegateクラスのメンバー変数を参照する
    let app:AppDelegate =
        (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    override func drawRect(rect: CGRect)
    {
        let x: CGFloat = 0.0
        let y: CGFloat = rect.size.height / 2
        
        let x2: CGFloat = rect.size.width
        let y2: CGFloat = 0.0
        
        let x3: CGFloat = rect.size.width
        let y3: CGFloat = rect.size.height
        
        UIColor.blackColor().setStroke()
        
        let line = UIBezierPath()
        line.lineWidth = 0.5
        line.moveToPoint(CGPointMake(x, y))
        line.addLineToPoint(CGPointMake(x2, y2))
        line.addLineToPoint(CGPointMake(x3, y3))
        line.addLineToPoint(CGPointMake(x, y))
        line.stroke()
        
        UIColor.clearColor().setFill();
        line.fill();
    }
    
    func draw(bcolor: String!) {
        let rFromIdx = bcolor.startIndex.advancedBy(0)
        let rToIdx = bcolor.startIndex.advancedBy(2)
        let gFromIdx = bcolor.startIndex.advancedBy(3)
        let gToIdx = bcolor.startIndex.advancedBy(5)
        let bFromIdx = bcolor.startIndex.advancedBy(6)
        let bToIdx = bcolor.startIndex.advancedBy(8)
        
        let ri = Int((bcolor.substringWithRange(rFromIdx...rToIdx)))
        let r = CGFloat(ri!)
        
        let gi = Int((bcolor.substringWithRange(gFromIdx...gToIdx)))
        let g = CGFloat(gi!)
        
        let bi = Int((bcolor.substringWithRange(bFromIdx...bToIdx)))
        let b = CGFloat(bi!)
        UIColor(red:r/255,green:g/255,blue:b/255,alpha:1.0).setFill();
    }
}