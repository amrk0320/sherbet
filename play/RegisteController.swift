//
//  ViewController.swift
//  play
//
//  Created by munetomoissei on 2016/05/04.
//  Copyright © 2016年 munetomoissei. All rights reserved.
//

import UIKit
import NCMB
import Firebase


class RegisteController: UIViewController {
    
    // username テキストボックス
    @IBOutlet weak var username: UITextField!
    
    // password テキストボックス
    @IBOutlet weak var password: UITextField!
    
    // エラーメッセージ1
    @IBOutlet weak var message1: UILabel!
    
    // エラーメッセージ2
    @IBOutlet weak var message2: UILabel!
    
    // エラーメッセージ3
    @IBOutlet weak var message3: UILabel!

    // logo画像
    @IBOutlet weak var logo: UIImageView!
    
    @IBOutlet weak var navgationItem: UINavigationItem!
    var logoImageView: UIImageView!
    
    var statusCode:String = ""
    
    var timer:NSTimer = NSTimer()
    // 端末内のデータ
    let app:AppDelegate =
        (UIApplication.sharedApplication().delegate as! AppDelegate)
    var refRoomApply: Firebase = Firebase(url: "https://playroom.firebaseio.com/")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //imageView作成
        self.logoImageView = UIImageView(frame: CGRectMake(0, 0, 172, 96))
        self.logoImageView.center = self.view.center
        //logo設定
        self.logoImageView.image = UIImage(named: "img/destiny-logo.png")
        //viewに追加
        self.view.addSubview(self.logoImageView)
        
        // usernameに下枠線を追加
        let border = CALayer()
        border.frame = CGRectMake(0.0, username.frame.size.height-1.0, username.frame.size.width, 1.0)
        border.backgroundColor = UIColor.whiteColor().CGColor
        username.layer.addSublayer(border)
  
        // passwordに下枠線を追加
        let tborder = CALayer()
        tborder.frame = CGRectMake(0.0, password.frame.size.height-1.0, password.frame.size.width, 1.0)
        tborder.backgroundColor = UIColor.whiteColor().CGColor
        password.layer.addSublayer(tborder)
        password.secureTextEntry = true

        
        // usernameの値が変化された時
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.textDidChange(_:)), name: UITextFieldTextDidChangeNotification, object: self.username)
        // passwordの値が変化された時
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.textDidChange(_:)), name: UITextFieldTextDidChangeNotification, object: self.password)
        
        // ナビゲーションボタンを追加
        let editBtn  = UIButton(type: UIButtonType.Custom)
        editBtn.enabled = false
        editBtn.frame = CGRectMake(0, 0, 50, 30)
        editBtn.backgroundColor = UIColor(red:80.0/255,green:127.0/255,blue:212.0/255,alpha:1.0)
        editBtn.setTitle("Next", forState: UIControlState.Normal)
        editBtn.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        editBtn.addTarget(self, action: #selector(self.register), forControlEvents: UIControlEvents.TouchUpInside)
        let editBarBtn = UIBarButtonItem(customView: editBtn)
        navgationItem.setRightBarButtonItem(editBarBtn, animated: true)
        navgationItem.rightBarButtonItem?.enabled = false

    }
    
    
    func textDidChange(notification: NSNotification) {
        // エラーメッセージを表示する
        self.message1.text = "username must be 4 to 12 characters long"
        self.message2.text = "password must be 8 to 12 characters long"
        UIView.animateWithDuration(0.25, animations: { () -> Void in
            self.message1.alpha = 1.0
            self.message2.alpha = 1.0
        })

        let usernameLength = username.text!.characters.count
        let passwordLength = password.text!.characters.count
        var usernameFlg:Int? = nil
        var passwordFlg:Int? = nil



        // usernameが4文字以上なら
        if usernameLength > 4 && usernameLength <= 12 {
            self.message1.text = "username must be 4 to 12 characters long ✔︎"
            self.message1.textColor = UIColor.whiteColor()
            usernameFlg = 1
        } else{
            self.message1.textColor = UIColor(red:60.0/255,green:95.0/255,blue:158.0/255,alpha:1.0)
            usernameFlg = nil

        }
        // passwordが4文字以上なら
        if passwordLength >= 8 && passwordLength <= 12 {
            self.message2.text = "password must be 8 to 12 characters long ✔︎"
            self.message2.textColor = UIColor.whiteColor()
            passwordFlg = 1
        } else{
            self.message2.textColor = UIColor(red:60.0/255,green:95.0/255,blue:158.0/255,alpha:1.0)
            passwordFlg = nil
        }
        // usernameとpasswordが両方正しく入力されたら
        if usernameFlg == 1 && passwordFlg == 1{
            // フェードイン
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                let view = self.navgationItem.rightBarButtonItem?.customView
                view?.alpha = 1
                self.navgationItem.rightBarButtonItem?.enabled = true
            })
        }else{
            self.navgationItem.rightBarButtonItem?.enabled = false
        }
        
    }
    
    // アニメーション記述メソッド
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        let view = navgationItem.rightBarButtonItem?.customView
        view?.hidden = false
        view?.alpha = 0
        
        // フェードイン
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.username.alpha = 1.0
            self.password.alpha = 1.0

        })
        
        //少し縮小するアニメーション
        UIView.animateWithDuration(0.3,
                                   delay: 1.0,
                                   options: UIViewAnimationOptions.CurveEaseOut,
                                   animations: { () in
                                    self.logoImageView.transform = CGAffineTransformMakeScale(0.9, 0.9)
            }, completion: { (Bool) in
                
        })
        
        //拡大させて、消えるアニメーション
        UIView.animateWithDuration(0.2,
                                   delay: 1.3,
                                   options: UIViewAnimationOptions.CurveEaseOut,
                                   animations: { () in
                                    self.logoImageView.transform = CGAffineTransformMakeScale(1.2, 1.2)
                                    self.logoImageView.alpha = 0
            }, completion: { (Bool) in
                self.logoImageView.removeFromSuperview()
        })
    }
    
    // API取得の開始処理
    func connectAPI() {
        
        // NCMBよりユーザー情報を取得する
        let query = NCMBQuery(className: "User")
        query.whereKey("userName", equalTo: self.username.text)
        query.findObjectsInBackgroundWithBlock { (objects: [AnyObject]!, error: NSError!) in
            if (error == nil) {
                if(objects.count > 0) {
                    
                    self.message3.alpha = 1.0
                    self.message3.text = "username is already used."
                    self.message3.textColor = UIColor.redColor()
                    
                    let message = objects[0].objectForKey("room") as! NSArray
                    print("[FIND] \(message)")
                    self.statusCode = "duplicate"
                } else {
                    var saveError: NSError?
                    let obj = NCMBObject(className: "User")
                    obj.setObject(self.username.text, forKey: "userName")
                    obj.setObject(self.password.text, forKey: "password")
                    obj.setObject(self.password.text, forKey: "sequence")
                    obj.setObject([""], forKey: "room")
                    obj.save(&saveError)
                    
                    // 次回ログイン用に端末にデータを保存する
                    let username = self.username.text
                    
                    // 保存
                    NSUserDefaults.standardUserDefaults().setObject(username, forKey: "username")
                    print("[SAVE] Done.")
                    self.statusCode = "success"
                    
                    NSUserDefaults.standardUserDefaults().setObject(self.app.name, forKey: "userName")
                    NSUserDefaults.standardUserDefaults().setObject(self.app.userImage, forKey: "userImage")
                    self.toMain()
                    print("1")
                }
            }
        }
    }
    
    func register(){
        //searchButtonを押した際の処理を記述
        print("登録するよ")
        connectAPI()
    }
    
    
    func toMain(){
        
        //LoadingProxy.off();

        
        //メイン画面へ遷移
        // 遷移するViewを定義する.このas!はswift1.2では as?だったかと。
        let mainViewController: TabbarController = self.storyboard?.instantiateViewControllerWithIdentifier("tab") as! TabbarController
        print(self.app.topRoomName)
        print("2")
        // Viewの移動する.
        self.presentViewController( mainViewController, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

