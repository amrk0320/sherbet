//
//  ViewController.swift
//  play
//
//  Created by munetomoissei on 2016/05/04.
//  Copyright © 2016年 munetomoissei. All rights reserved.
//

import UIKit
import NCMB
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit


class WelcomeViewController: UIViewController {
    // 端末内のデータ
    let app:AppDelegate =
        (UIApplication.sharedApplication().delegate as! AppDelegate)
    var refRoomApply: Firebase = Firebase(url: "https://playroom.firebaseio.com/")
    var userProfile : NSDictionary!
    var userID : String!
    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var title1: UILabel!

    @IBOutlet weak var headTopMargin: NSLayoutConstraint!
    
    @IBOutlet weak var heightConst: NSLayoutConstraint!
    var refUser: Firebase = Firebase(url: "https://playuser.firebaseio.com/")

 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // autolayautoを設定する
        let height = self.view.frame.height * 0.2
        heightConst.constant = height
        headTopMargin.constant = height
        
        self.title1.alpha = 0.0
        self.view.alpha = 0.0
        
        // Do any additional setup after loading the view, typically from a nib.
        //グラデーションの開始色
        let topColor = UIColor(red:5.0/255,green:5.0/255,blue:5.0/255,alpha:1.0)
        //グラデーションの末端色
        let bottomColor = self.app.lightBluekColor
        
        //グラデーションの色を配列で管理
        let gradientColors: [CGColor] = [topColor.CGColor, bottomColor.CGColor]
        
        //グラデーションレイヤーを作成
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        
        //グラデーションの色をレイヤーに割り当てる
        gradientLayer.colors = gradientColors
        //グラデーションレイヤーをスクリーンサイズにする
        gradientLayer.frame = self.view.bounds
        
        //グラデーションレイヤーをビューの一番下に配置
        self.view.layer.insertSublayer(gradientLayer, atIndex: 0)
        
        header.backgroundColor = UIColor(red:5.0/255,green:5.0/255,blue:5.0/255,alpha:1.0)
    }
    
    override func viewDidAppear(animated: Bool) {
        
        // フェードイン
        UIView.animateWithDuration(1.0, animations: { () -> Void in
            self.title1.alpha = 1.0
            self.view.alpha = 1.0
        })
        
        if (FBSDKAccessToken.currentAccessToken() != nil) {
            print("User Already Logged In")
        } else {

        }
    }
    @IBAction func tapLogin(sender: AnyObject) {
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.logInWithReadPermissions(["public_profile", "email", "user_friends"], fromViewController: self) { (result, error) in
            if (error != nil) {
                // エラーが発生した場合
                print("Process error")
            } else if result.isCancelled {
                // ログインをキャンセルした場合
                print("Cancelled")
            } else {
                // その他
                print("Login Succeeded")
                self.connectAPI()
            }
        }
    }
    
    func returnUserData()
    {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me",
                                                                 parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"])
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
            if ((error) != nil)
            {
                // エラー処理
                print("Error: \(error)")
            }
            else
            {
                // プロフィール情報をディクショナリに入れる
                self.userProfile = result as! NSDictionary
                print(self.userProfile)
                
                // プロフィール画像の取得（よくあるように角を丸くする）
                let profileImageURL : String = self.userProfile.objectForKey("picture")?.objectForKey("data")?.objectForKey("url") as! String
                self.app.userImage = UIImage(data: NSData(contentsOfURL: NSURL(string: profileImageURL)!)!)
                
                //名前とemail
                self.app.name = self.userProfile.objectForKey("name") as? String
                print("1")
                self.userID = self.userProfile.objectForKey("id") as? String
            }
        })
        
    }
    func trimPicture(rawPic:UIImage) -> UIImage {
        let rawImageW = rawPic.size.width
        let rawImageH = rawPic.size.height
        
        let posX = (rawImageW - 200) / 2
        let posY = (rawImageH - 200) / 2
        let trimArea : CGRect = CGRectMake(posX, posY, 200, 200)
        
        let rawImageRef:CGImageRef = rawPic.CGImage!
        let trimmedImageRef = CGImageCreateWithImageInRect(rawImageRef, trimArea)
        let trimmedImage : UIImage = UIImage(CGImage : trimmedImageRef!)
        return trimmedImage
    }
    
    // API取得の開始処理
    func connectAPI() {
        
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me",
                                                                 parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"])
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
            if ((error) != nil)
            {
                // エラー処理
                print("Error: \(error)")
            }
            else
            {
                // プロフィール情報をディクショナリに入れる
                self.userProfile = result as! NSDictionary
                print(self.userProfile)
                
                // プロフィール画像の取得（よくあるように角を丸くする）
                let profileImageURL : String = self.userProfile.objectForKey("picture")?.objectForKey("data")?.objectForKey("url") as! String
                self.app.userImage = UIImage(data: NSData(contentsOfURL: NSURL(string: profileImageURL)!)!)
                
                //名前とemail
                self.app.name = self.userProfile.objectForKey("name") as? String
                print("1")
                self.userID = self.userProfile.objectForKey("id") as? String
                
                // firebaseにトークの作成を通知する
                //firebaseにデータを送信、保存する
                let dictionary = ["userName":self.app.name!]
                let post1Ref = self.refUser.childByAutoId()
                post1Ref.setValue(dictionary)
                print("2")
                
                // 次回ログイン用に端末にデータを保存する
                let username = self.app.name
                
                self.refUser.queryOrderedByChild("userName").queryEqualToValue(username).queryLimitedToLast(1).observeEventType(FEventType.ChildAdded, withBlock: { (snapshot) in
                    
                    print(snapshot)
                    
                    let id = snapshot.key
                    self.app.userId =  id
                    // 保存
                    print(self.app.userId)
                    NSUserDefaults.standardUserDefaults().setObject(self.app.userId, forKey: "userId")
                    NSUserDefaults.standardUserDefaults().setObject(username, forKey: "username")
                    print("[SAVE] Done.")
                    NSUserDefaults.standardUserDefaults().setObject(self.app.name, forKey: "userName")
                    // NSDate型に変換して画像を端末に保存する
                    let imageData = UIImageJPEGRepresentation(self.app.userImage, 1);
                    NSUserDefaults.standardUserDefaults().setObject(imageData, forKey: "userImage")
                    NSUserDefaults.standardUserDefaults().synchronize()
                    self.toMain()
        
                })
            }
        })

    }

    func toMain(){
        //メイン画面へ遷移
        // 遷移するViewを定義する.このas!はswift1.2では as?だったかと。
        let mainViewController: TabbarController = self.storyboard?.instantiateViewControllerWithIdentifier("tab") as! TabbarController
        self.presentViewController( mainViewController, animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

