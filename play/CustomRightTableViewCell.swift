//
//  CustomLeftTableViewCell.swift
//  play
//
//  Created by munetomoissei on 2016/05/18.
//  Copyright © 2016年 munetomoissei. All rights reserved.
//

import Foundation
import UIKit

class CustomRightTableViewCell: UITableViewCell
{
    var icon: UIImageView = UIImageView() // アイコン
    var name: UILabel = UILabel() // 名前
    var created: UILabel = UILabel() // 投稿日時
    
    var arrow: CustomLeftArrow = CustomLeftArrow() // 吹き出しの突起の部分
    var message: UILabel = UILabel() // 吹き出しの文字を表示している部分
    var viewMessage: UIView = UIView() // 吹き出しの枠部分
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)!
        self.selectionStyle = UITableViewCellSelectionStyle.None
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
        self.viewMessage.layer.borderColor = UIColor(red:30.0/255,green:30.0/255,blue:30.0/255,alpha:1.0).CGColor
        self.viewMessage.layer.borderWidth = 0.5
        self.viewMessage.layer.cornerRadius = 10.0

        self.message.numberOfLines = 0
        
        self.viewMessage.addSubview(self.message)
        self.contentView.addSubview(self.viewMessage)
        
        self.created.font = UIFont.systemFontOfSize(11)
        self.created.textColor = UIColor(red:184.0/255,green:234.0/255,blue:96.0/255,alpha:1.0)
        self.created.textAlignment = NSTextAlignment.Right
        self.addSubview(self.created)
        
    }
    
    func setData(widthMax: CGFloat,data: Dictionary<String,AnyObject>) -> CGFloat
    {
        let marginLeft: CGFloat = 60
        let marginRight: CGFloat = 0
        let marginVertical: CGFloat = 10
        
        
        let font = data["font"] as? String
        if font != nil{
            self.message.font = UIFont(name:font!,size:50)
        }
        
        
        // 投稿日時
        let xCreated: CGFloat = self.icon.frame.origin.x + self.icon.frame.size.width + 3
        let yCreated: CGFloat = self.icon.frame.origin.y
        let widthCreated: CGFloat = widthMax - (self.icon.frame.origin.x + self.icon.frame.size.width + 10)
        let heightCreated: CGFloat = 30
        let date = data["created"] as? NSString
        self.created.text = date!.substringFromIndex(11)
        self.created.frame = CGRectMake(xCreated-120, yCreated, widthCreated, heightCreated)
        
        let paddingHorizon: CGFloat = 10
        let paddingVertical: CGFloat = 10
        
        let widthLabelMax: CGFloat = widthMax - (marginLeft + marginRight + paddingHorizon * 2)
        
        let xMessageLabel: CGFloat = paddingHorizon
        let yMessageLabel: CGFloat = paddingVertical
        
        self.message.frame = CGRectMake(xMessageLabel, yMessageLabel, widthLabelMax, 0)
        self.message.text = data["text"] as? String
        self.message.sizeToFit()
        
        let xMessageView: CGFloat = marginLeft
        let yMessageView: CGFloat = marginVertical
        let widthMessageView: CGFloat = self.message.frame.size.width + paddingHorizon * 2
        let heightMessageView: CGFloat = self.message.frame.size.height + paddingVertical * 2
        self.viewMessage.frame = CGRectMake(xMessageView+180, yMessageView, widthMessageView, heightMessageView)
        
        let height: CGFloat = self.viewMessage.frame.height + marginVertical * 2
        
        let bcolor = data["bColor"] as? String
        let rFromIdx = bcolor!.startIndex.advancedBy(0)
        let rToIdx = bcolor!.startIndex.advancedBy(2)
        let gFromIdx = bcolor!.startIndex.advancedBy(3)
        let gToIdx = bcolor!.startIndex.advancedBy(5)
        let bFromIdx = bcolor!.startIndex.advancedBy(6)
        let bToIdx = bcolor!.startIndex.advancedBy(8)
        
        let ri = Int((bcolor?.substringWithRange(rFromIdx...rToIdx))!)
        let r = CGFloat(ri!)
        
        let gi = Int((bcolor?.substringWithRange(gFromIdx...gToIdx))!)
        let g = CGFloat(gi!)
        
        let bi = Int((bcolor?.substringWithRange(bFromIdx...bToIdx))!)
        let b = CGFloat(bi!)
        
        self.viewMessage.backgroundColor = UIColor(red:r/255,green:g/255,blue:b/255,alpha:1.0)
        self.message.backgroundColor = UIColor(red:r/255,green:g/255,blue:b/255,alpha:1.0)
        
        let tColor = data["tColor"] as? String
        let rtFromIdx = tColor!.startIndex.advancedBy(0)
        let rtToIdx = tColor!.startIndex.advancedBy(2)
        let gtFromIdx = tColor!.startIndex.advancedBy(3)
        let gtToIdx = tColor!.startIndex.advancedBy(5)
        let btFromIdx = tColor!.startIndex.advancedBy(6)
        let btToIdx = tColor!.startIndex.advancedBy(8)
        
        let rti = Int((tColor?.substringWithRange(rtFromIdx...rtToIdx))!)
        let rt = CGFloat(rti!)
        
        let gti = Int((tColor?.substringWithRange(gtFromIdx...gtToIdx))!)
        let gt = CGFloat(gti!)
        
        let bti = Int((tColor?.substringWithRange(btFromIdx...btToIdx))!)
        let bt = CGFloat(bti!)
        
        self.message.textColor = UIColor(red:rt/255,green:gt/255,blue:bt/255,alpha:1.0)

        
        return height
    }
}
