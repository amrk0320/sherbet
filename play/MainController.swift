//
//  MainController.swift
//  play
//
//  Created by munetomoissei on 2016/05/11.
//  Copyright © 2016年 munetomoissei. All rights reserved.
//

import UIKit
import QuartzCore
import AVFoundation
import Firebase
import NCMB
import FontAwesome_swift

class MainController: UIViewController,UITextViewDelegate ,AVAudioPlayerDelegate,UIGestureRecognizerDelegate{
    // テキストビュー
    @IBOutlet weak var textView: UIView!

    // テキストフィールド
    @IBOutlet weak var textField: UITextView!
    // 上側のview
    @IBOutlet weak var tabelView: UITableView!
    // テキストフィールドの高さ
    @IBOutlet weak var textHeightConstarain: NSLayoutConstraint!
    // メッセージ
    var tableData: [Dictionary<String,AnyObject>]!
    // 送信ボタン
    @IBOutlet weak var sendButton: UIButton!
    // セルのインスタンスを作成(高さ取得メソッドを使う為)
    var heightLeftCell: CustomLeftTableViewCell = CustomLeftTableViewCell()
    var heightRightCell: CustomRightTableViewCell = CustomRightTableViewCell()
    
    var AudioPlayer = AVAudioPlayer ()
    // firebaseセットアップ
    var ref: Firebase!
    // username
    var name:String = ""
    // 下側pageMenu2
    var pageMenu2 : CAPSPageMenu?
    // 上部のコントローラー
    var talk: UIViewController!
    // room
    let refRoom: Firebase = Firebase(url: "https://playroom.firebaseio.com/")
    // チャット相手の一覧を生成
    let app:AppDelegate =
        (UIApplication.sharedApplication().delegate as! AppDelegate)    
    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var logo: UIImageView!
   
    @IBOutlet weak var tabitem: UITabBarItem!
    var logoImageView: UIImageView!

    @IBOutlet weak var addButton: UIImageView!
    // ヘッダーロゴの大きさ
    @IBOutlet weak var logoH: NSLayoutConstraint!
    @IBOutlet weak var logoW: NSLayoutConstraint!
    // tableViewの横幅
    @IBOutlet weak var tWidth: NSLayoutConstraint!
    //　textFieldの横幅
    @IBOutlet weak var textFWidth: NSLayoutConstraint!
    // textFieldの左スペース
    @IBOutlet weak var tFieldLMargin: NSLayoutConstraint!
    // textViewの横幅
    @IBOutlet weak var textViewWidth: NSLayoutConstraint!
    
    @IBOutlet weak var sendButtonWidth: NSLayoutConstraint!

    @IBOutlet weak var sendButtonRightMargin: NSLayoutConstraint!
    
    @IBOutlet weak var headerW: NSLayoutConstraint!
    @IBOutlet weak var headerH: NSLayoutConstraint!
    @IBOutlet weak var logoTopMargin: NSLayoutConstraint!
    // 関係ないところを触るとギーボードを閉じる
    @IBAction func tapScreen(sender
        : AnyObject) {
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // autolayautoを設定する
        let tHeight = self.view.frame.height * 0.11
        let lHeight = self.view.frame.height * 0.1
        let lWidth = self.view.frame.width * 0.4
        
        headerW.constant = self.view.frame.width
        headerH.constant = tHeight
        logoH.constant = lHeight
        logoW.constant = lWidth
        logoTopMargin.constant = lHeight*0.1
        
        self.app.logoW = lWidth
        self.app.logoH = lHeight
        self.app.headerH = tHeight
        self.app.logoTopMargin = lHeight*0.1
        
        tWidth.constant = self.view.frame.width
        textFWidth.constant = self.view.frame.width*0.8
        textViewWidth.constant = self.view.frame.width
        sendButtonRightMargin.constant = self.view.frame.width*0.0005
        sendButtonWidth.constant = self.view.frame.width*0.15
        
        self.tabBarItem.tag = 1
        self.tabBarItem.badgeValue = "1"
        
        // テキストボックスの色を変更
        self.textView.backgroundColor = UIColor.flatDarkWhiteColor()
        self.textField.backgroundColor = UIColor.flatWhiteColor()
        
        self.view.backgroundColor = UIColor.flatWhiteColor()
        self.tabelView.backgroundColor = UIColor.flatWhiteColor()
        
        // スプラッシュアニメーションを作る
        //imageView作成
        self.logoImageView = UIImageView(frame: CGRectMake(0, 0, 172, 96))
        self.logoImageView.center = self.view.center
        //logo設定
        self.logoImageView.image = UIImage(named: "img/destiny-logo.png")
        //viewに追加
        self.view.addSubview(self.logoImageView)
        
        let pinchGesture:UIPinchGestureRecognizer=UIPinchGestureRecognizer(target:self, action: #selector(self.pinch))
        self.tabelView!.addGestureRecognizer(pinchGesture)
        
        
        self.tabelView.alpha = 0.0
        
        self.addButton.image = UIImage.fontAwesomeIconWithName(.HandScissorsO, textColor:UIColor.whiteColor(),size: CGSizeMake(20, 20))
        
        let logoImg = UIImage(named: "img/destiny-logo.png")
        //        // 画像をUIImageViewに設定する.
        self.logo.image = logoImg
        header.backgroundColor = self.app.headerColor
        
        // tabitemの編集
        let image = UIImage.fontAwesomeIconWithName(.HandPeaceO, textColor:UIColor.whiteColor(),size: CGSizeMake(50, 50))
        self.tabBarItem.image = image
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MainController.keyboardWillShow), name: UIKeyboardWillShowNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MainController.keyboardWillHide), name: UIKeyboardWillHideNotification, object: nil)
        
        textField.delegate = self
        textField.layer.cornerRadius = 3
        
        // サウンドを設定する
        let soundFilePath = NSURL(fileURLWithPath:NSBundle.mainBundle().pathForResource("sound/send", ofType: "mp3")!)
        AudioPlayer = try! AVAudioPlayer(contentsOfURL: soundFilePath);
        AudioPlayer.prepareToPlay()
        
        talk = (self.storyboard?.instantiateViewControllerWithIdentifier("talk"))! as UIViewController
        talk.view.frame = tabelView.bounds

        
        // 2
        addChildViewController(talk)

        
        // 3
        tabelView.addSubview(talk.view)

        
        // 4
        
        // VIEWをセットします
        //setView()
        
        let border = CALayer()
        border.frame = CGRectMake(0.0, tabelView.frame.height-1.0, tabelView.frame.size.width, 2.0)
        border.backgroundColor = UIColor(red:80.0/255,green:80.0/255,blue:80.0/255,alpha:1.0).CGColor
        tabelView.layer.addSublayer(border)
        
        tabelView.backgroundColor = UIColor(red:30.0/255,green:30.0/255,blue:30.0/255,alpha:1.0)
        tabelView.layer.cornerRadius = 3.0
        
        // タップイベントを追加させる
        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.add))
        self.addButton.userInteractionEnabled = true
        self.addButton.addGestureRecognizer(doubleTapGesture)
        
    }
    
    func keyboardWillShow(notification: NSNotification?) {
        // キーボード表示時の動作をここに記述する
        let rect = (notification?.userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        let duration:NSTimeInterval = notification?.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! Double
        
        UIView.animateWithDuration(duration, animations: {
            let transform = CGAffineTransformMakeTranslation(0, -rect.size.height+self.app.tabBarHeight)
            self.textView.transform = transform
            },completion:nil)
        
    }
    
    func keyboardWillHide(notification: NSNotification?) {
        // キーボード消滅時の動作をここに記述する
        let duration:NSTimeInterval = notification?.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! Double
        UIView.animateWithDuration(duration, animations:{
            self.textView.transform = CGAffineTransformIdentity
            },completion:nil)

    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        //非表示にする。
        if(textField.isFirstResponder()){
            textField.resignFirstResponder()
        }
        
    }
    
    // テキストが変更されたらテキストボックスの高さを可変にする
    func textViewDidChange(textView: UITextView) {
        let maxHeight = CGFloat(55.0) // 入力フィールドの最大サイズ
        let text = textField.text
        if text.characters.count > 0 {
            sendButton.setTitle("Send", forState: .Normal)
        } else {
            sendButton.setTitle("", forState: .Normal)
        }
        if(textView.frame.size.height < maxHeight) {
            let size:CGSize = textView.sizeThatFits(textView.frame.size)
            textHeightConstarain.constant = size.height
        }
    }
    
    // 送信がクリック
    @IBAction func send(sender: UIButton) {
        let text = textField.text
        if text.characters.count > 0 {
            AudioPlayer.play()
            
            // 送信するチャットルームを振り分け
            // 上下のチャットルームを振り分け
            let tApp:AppDelegate =
                (UIApplication.sharedApplication().delegate as! AppDelegate)
            let selectedRoom = tApp.selectedRoom
            
            if selectedRoom == 0 {
                let top1ViewController = Top1ViewController()
                top1ViewController.addText(text)
                textField.text = nil
                sendButton.setTitle("", forState: .Normal)
            } else if selectedRoom == 1 {
                let top2ViewController = Top2ViewController()
                top2ViewController.addText(text)
                textField.text = nil
                sendButton.setTitle("", forState: .Normal)
            }
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
//        NSNotificationCenter.defaultCenter().removeObserver(self,name: UIKeyboardWillShowNotification,object: nil)
//        NSNotificationCenter.defaultCenter().removeObserver(self,name: UIKeyboardWillHideNotification,object: nil)
    }
    
    // テキストが変更されたらテキストボックスの高さを可変にする
    func keyBoardhidden(bool: Bool) {
        self.textField.hidden = bool

    }
    
    // アニメーション記述メソッド
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // フェードイン
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.tabelView.alpha = 1.0
        })
        
        //少し縮小するアニメーション
        UIView.animateWithDuration(0.3,
                                   delay: 1.0,
                                   options: UIViewAnimationOptions.CurveEaseOut,
                                   animations: { () in
                                    self.logoImageView.transform = CGAffineTransformMakeScale(0.9, 0.9)
            }, completion: { (Bool) in
                
        })
        
        //拡大させて、消えるアニメーション
        UIView.animateWithDuration(0.2,
                                   delay: 1.3,
                                   options: UIViewAnimationOptions.CurveEaseOut,
                                   animations: { () in
                                    self.logoImageView.transform = CGAffineTransformMakeScale(1.2, 1.2)
                                    self.logoImageView.alpha = 0
            }, completion: { (Bool) in
                self.logoImageView.removeFromSuperview()
        })
    }
    
    func pinch() {
        let alert: UIAlertController = UIAlertController(title: "ルーム削除", message: "このトークルームを削除してもいいですか？", preferredStyle:  UIAlertControllerStyle.Alert)
        
        let defaultAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:{

            (action: UIAlertAction!) -> Void in
            // トークルーム削除
            
            // 配列を入れ替え
            var tmpTopRoom: [String] = []
            var tmpTopRoomName: [String] = []
            
            for (index, room) in self.app.topRoom.enumerate() {
                if index != self.app.selectedRoom {
                    tmpTopRoom.append(room)
                }
            }
            
            for (index, roomNm) in self.app.topRoomName.enumerate() {
                if index != self.app.selectedRoom {
                    tmpTopRoomName.append(roomNm)
                }
            }
            
            self.app.topRoom = tmpTopRoom
            self.app.topRoomName = tmpTopRoomName
            
            if self.app.selectedRoom == 0 {
                if self.app.top1tableData.count > 0 {
                    // 端末のトーク履歴を削除
                    self.app.top1tableData = []
                    NSUserDefaults.standardUserDefaults().setObject(self.app.top1tableData, forKey: "top1")
                    self.app.top1tableView.reloadData()
                }
            }
            NSUserDefaults.standardUserDefaults().setObject(self.app.topRoom, forKey: "topRoom")
            NSUserDefaults.standardUserDefaults().setObject(self.app.topRoomName, forKey: "topRoomName")
            let mainViewController: TabbarController = self.storyboard?.instantiateViewControllerWithIdentifier("tab") as! TabbarController
            self.presentViewController( mainViewController, animated: false, completion: nil)
        })
        // キャンセルボタン
        let cancelAction: UIAlertAction = UIAlertAction(title: "キャンセル", style: UIAlertActionStyle.Cancel, handler:{
            // ボタンが押された時の処理を書く（クロージャ実装）
            (action: UIAlertAction!) -> Void in
        })
        
        alert.addAction(cancelAction)
        alert.addAction(defaultAction)
        
        presentViewController(alert, animated: true, completion: nil)
    }
    
    func add() {
        // チャット追加の画面へ
        let addChatViewController: AddChatViewController = self.storyboard?.instantiateViewControllerWithIdentifier("addChat") as! AddChatViewController
        
        // Viewの移動する.
        self.presentViewController( addChatViewController, animated: true, completion: nil)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
