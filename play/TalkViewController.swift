//
//  Vc1ViewController.swift
//  play
//
//  Created by munetomoissei on 2016/06/01.
//  Copyright © 2016年 munetomoissei. All rights reserved.
//

import UIKit
import Firebase

class TalkViewController: UIViewController {
    
    // 上側pageMenu
    var pageMenu1 : CAPSPageMenu?
    // room
    var refRoom: Firebase = Firebase(url: "https://playroom.firebaseio.com/")
    // チャット相手の一覧を生成
    let app:AppDelegate =
        (UIApplication.sharedApplication().delegate as! AppDelegate)
    var roomName1 :String?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.flatWhiteColor()
        
        // pageMenuの設定
        self.title = "PAGE MENU"
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30.0/255.0, green: 30.0/255.0, blue: 30.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.barStyle = UIBarStyle.Black
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 70.0/255.0, alpha: 1.0)]

        refRoom.queryOrderedByChild("toUserId").queryStartingAtValue(app.userId).queryLimitedToLast(1).observeEventType(FEventType.ChildAdded, withBlock: { (snapshot) in
                
                //topRoomとtoproomNameとfriendsに追加
                let fromUser = snapshot.value["fromUser"] as? String
                let senderId = snapshot.value["senderId"] as? String
                let action = snapshot.value["action"] as? String
                let id = snapshot.value["id"] as? String
                print(fromUser)
                // 取得していないユーザーからの送信なら
                if self.app.inviteId != id {
                    
                    self.app.inviteId = id
                    NSUserDefaults.standardUserDefaults().setObject(self.app.inviteId, forKey: "invitedId")
                    
                    if action == "invite" {
                        var hasCount = 0
                        let roomID  = snapshot.value["id"] as? String
                        // 友達が１人いる場合
                        if self.app.chat != nil {
                            for (_, name) in self.app.chat.enumerate() {
                                if name["toUserId"] == senderId {
                                    hasCount = 1
                                    break
                                }
                            }
                            // 新規の友達ならchatに追加する
                            if hasCount == 0 {
                                self.app.chat.append(["roomID": roomID!, "toUser": fromUser!, "toUserId": senderId!])
                            }
                            // タブが５つ以下だったら
                            if self.app.topRoom.count < 5 {
                                self.app.topRoom.append(roomID!)
                                self.app.topRoomName.append(fromUser!)
                            } else {
                                // 5つ以上だったらトークルームを入れ替え
                                // 配列を入れ替え
                                var tmpTopRoom: [String] = []
                                var tmpTopRoomName: [String] = []
                                
                                tmpTopRoom.append(roomID!)
                                tmpTopRoomName.append(fromUser!)
                                
                                for (index, room) in self.app.topRoom.enumerate() {
                                    if index < 5 {
                                        tmpTopRoom.append(room)
                                    }
                                }
                                
                                for (index, roomNm) in self.app.topRoomName.enumerate() {
                                    if index < 5 {
                                        tmpTopRoomName.append(roomNm)
                                    }
                                }
                                
                                self.app.topRoom = tmpTopRoom
                                self.app.topRoomName = tmpTopRoomName
                            }
                            // 端末のデータを更新する
                            NSUserDefaults.standardUserDefaults().setObject(self.app.topRoom, forKey: "topRoom")
                            NSUserDefaults.standardUserDefaults().setObject(self.app.topRoomName, forKey: "topRoomName")
                            NSUserDefaults.standardUserDefaults().setObject(self.app.chat, forKey: "chat")
                            let mainViewController: TabbarController = self.storyboard?.instantiateViewControllerWithIdentifier("tab") as! TabbarController
                            self.presentViewController( mainViewController, animated: false, completion: nil)
                        } else {
                            // 友達が１人もいない場合
                            self.app.topRoom.append(roomID!)
                            self.app.topRoomName.append(fromUser!)
                            self.app.chat = []
                            self.app.chat.append(["roomID": roomID!, "toUser": fromUser!, "toUserId": senderId!])
                            // 端末のデータを更新する
                            NSUserDefaults.standardUserDefaults().setObject(self.app.topRoom, forKey: "topRoom")
                            NSUserDefaults.standardUserDefaults().setObject(self.app.topRoomName, forKey: "topRoomName")
                            NSUserDefaults.standardUserDefaults().setObject(self.app.chat, forKey: "chat")
                            let mainViewController: TabbarController = self.storyboard?.instantiateViewControllerWithIdentifier("tab") as! TabbarController
                            self.presentViewController( mainViewController, animated: false, completion: nil)
                        }
                    }
                }
            }
        )
        
        
        var controllerArray : [UIViewController] = []

        for (index, roomName) in app.topRoomName.enumerate() {
            
            if index == 0{
                let controller : UIViewController =  Top1ViewController()
                controller.title = roomName
                controllerArray.append(controller)
            } else if index == 1 {
                let controller : UIViewController =  Top2ViewController()
                controller.title = roomName
                controllerArray.append(controller)
            } else if index == 2 {
                let controller : UIViewController =  Top3ViewController()
                controller.title = roomName
                controllerArray.append(controller)
            } else if index == 3 {
                let controller : UIViewController =  Top4ViewController()
                controller.title = roomName
                controllerArray.append(controller)
            } else if index == 4 {
                let controller : UIViewController =  Top5ViewController()
                controller.title = roomName
                controllerArray.append(controller)
            }
        }
        
        // pageMenuのバーの設定
        let parameters: [CAPSPageMenuOption] = [
            .ScrollMenuBackgroundColor(self.app.blackColor),
            .ViewBackgroundColor(UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 1.0)),
            .SelectionIndicatorColor(UIColor.redColor()),
            .BottomMenuHairlineColor(UIColor(red: 40.0/255.0, green: 40.0/255.0, blue: 40.0/255.0, alpha: 1.0)),
            .MenuItemFont(UIFont(name: "HelveticaNeue", size: 13.0)!),
            .MenuHeight(50.0),
            .MenuItemWidth(80.0),
            .CenterMenuItems(false)
        ]
        
        pageMenu1 = CAPSPageMenu(viewControllers: controllerArray, frame: CGRectMake(0.0, 0.0, self.view.frame.width, self.view.frame.height), pageMenuOptions: parameters)
        
        
        self.addChildViewController(pageMenu1!)
        self.view.addSubview(pageMenu1!.view)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
