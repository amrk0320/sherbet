//
//  ChatSettingViewController.swift
//  play
//
//  Created by munetomoissei on 2016/07/03.
//  Copyright © 2016年 munetomoissei. All rights reserved.
//

import UIKit

class ChatSettingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    // テーブルビュー
    @IBOutlet weak var settingTable: UITableView!
    // セルに表示するテキスト
    let contents = ["バブルカラー", "テキストカラー","フォント","文字サイズ","背景"]
    private var bubbleColor: UITextField!
    private var tintColor: UITextField!
    private var font: UITextField!
    private var fontSize: UITextField!
    private var backGround: UITextField!
    var bubbleColorArr: NSArray = ["default","pink","white","green","purple","peach"]
    var tintColorArr: NSArray = ["default","green","pink","purple","peach"]
    var fontArr: NSArray = ["default","JK","夏","漫画"]
    var fontSizeArr: NSArray = ["12","24","50"]
    var backGroundArr: NSArray = ["default","pink","white","green","purple","peach"]
    var myToolBar: UIToolbar!
    // AppDelegateクラスのメンバー変数を参照する
    let app:AppDelegate =
        (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingTable.delegate = self
        settingTable.dataSource = self
        
        self.settingTable.separatorColor = UIColor.clearColor()
        self.settingTable.layoutMargins = UIEdgeInsetsZero
        self.settingTable.allowsSelection = false


    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // セルの行数
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contents.count
    }
    
    
    //セルの内容を変更
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "Cell")
        cell.layoutMargins = UIEdgeInsetsZero
        
        
        self.settingTable.backgroundColor = UIColor.flatWhiteColor()
        cell.backgroundColor = UIColor.flatWhiteColor()
        cell.textLabel?.text = contents[indexPath.row]
        cell.textLabel?.textColor = self.app.textColor
        cell.textLabel?.font = UIFont(name:"OmePlus-P-Gothic-light",size: 14)

        
        let line =  UIView(frame : CGRectMake(0.0, cell.frame.height-1.0, settingTable.frame.size.width, 1.0))
        line.backgroundColor = UIColor(red: 110.0/255.0, green: 110.0/255.0, blue: 110.0/255.0, alpha: 1.0)
        cell.addSubview(line)
        
        // テキストフィールドをセット
        // UITextFieldを作成する.
        if indexPath.row == 0 {
            bubbleColor = UITextField(frame: CGRectMake(200,cell.frame.height-3.0,110,25))
            bubbleColor.text = ""
            bubbleColor.delegate = self
            bubbleColor.borderStyle = UITextBorderStyle.RoundedRect
            bubbleColor.layer.position = CGPoint(x:cell.bounds.width-2,y:cell.frame.height/2);
            
            //ToolBar作成。ニョキ担当
            myToolBar = UIToolbar(frame: CGRectMake(0, self.view.frame.size.height/6, self.view.frame.size.width, 40.0))
            myToolBar.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-20.0)
            myToolBar.backgroundColor = UIColor.whiteColor()
            myToolBar.barStyle = UIBarStyle.Black
            myToolBar.tintColor = UIColor.whiteColor()
            
            //ToolBarを閉じるボタンを追加
            let closeBtn  = UIButton(type: UIButtonType.Custom)
            closeBtn.frame = CGRectMake(self.view.frame.width/10*9,0, 50, 30)
            closeBtn.layer.position = CGPoint(x: self.view.frame.width/10*9, y:myToolBar.frame.height/2)
            closeBtn.setTitle("ok", forState: UIControlState.Normal)
            closeBtn.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            closeBtn.addTarget(self, action: #selector(self.onClick(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            myToolBar.addSubview(closeBtn)
            
            // gender用のpickerView
            let genderPickerView = UIPickerView()
            genderPickerView.showsSelectionIndicator = true
            genderPickerView.delegate = self
            genderPickerView.dataSource = self
            bubbleColor!.inputView = genderPickerView
            bubbleColor.inputAccessoryView = myToolBar
            genderPickerView.tag = 1
            cell.addSubview(bubbleColor)
            
            
        } else if indexPath.row == 1{
            tintColor = UITextField(frame: CGRectMake(200,cell.frame.height-3.0,110,25))
            tintColor.text = ""
            tintColor.delegate = self
            tintColor.borderStyle = UITextBorderStyle.RoundedRect
            tintColor.layer.position = CGPoint(x:cell.bounds.width-2,y:cell.frame.height/2);
            
            //ToolBar作成。ニョキ担当
            myToolBar = UIToolbar(frame: CGRectMake(0, self.view.frame.size.height/6, self.view.frame.size.width, 40.0))
            myToolBar.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-20.0)
            myToolBar.backgroundColor = UIColor.whiteColor()
            myToolBar.barStyle = UIBarStyle.Black
            myToolBar.tintColor = UIColor.whiteColor()
            
            //ToolBarを閉じるボタンを追加
            let closeBtn  = UIButton(type: UIButtonType.Custom)
            closeBtn.frame = CGRectMake(self.view.frame.width/10*9,0, 50, 30)
            closeBtn.layer.position = CGPoint(x: self.view.frame.width/10*9, y:myToolBar.frame.height/2)
            closeBtn.setTitle("ok", forState: UIControlState.Normal)
            closeBtn.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            closeBtn.addTarget(self, action: #selector(self.onClick(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            myToolBar.addSubview(closeBtn)
            
            // age用のpickerView
            let agePickerView = UIPickerView()
            agePickerView.showsSelectionIndicator = true
            agePickerView.delegate = self
            agePickerView.dataSource = self
            tintColor!.inputView = agePickerView
            tintColor.inputAccessoryView = myToolBar
            agePickerView.tag = 2
            
            cell.addSubview(tintColor)
        } else if indexPath.row == 2{
            
            font = UITextField(frame: CGRectMake(200,cell.frame.height-3.0,110,25))
            font.text = ""
            font.delegate = self
            font.borderStyle = UITextBorderStyle.RoundedRect
            font.layer.position = CGPoint(x:cell.bounds.width-2,y:cell.frame.height/2);
            
            //ToolBar作成。ニョキ担当
            myToolBar = UIToolbar(frame: CGRectMake(0, self.view.frame.size.height/6, self.view.frame.size.width, 40.0))
            myToolBar.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-20.0)
            myToolBar.backgroundColor = UIColor.whiteColor()
            myToolBar.barStyle = UIBarStyle.Black
            myToolBar.tintColor = UIColor.whiteColor()
            
            //ToolBarを閉じるボタンを追加
            let closeBtn  = UIButton(type: UIButtonType.Custom)
            closeBtn.frame = CGRectMake(self.view.frame.width/10*9,0, 50, 30)
            closeBtn.layer.position = CGPoint(x: self.view.frame.width/10*9, y:myToolBar.frame.height/2)
            closeBtn.setTitle("ok", forState: UIControlState.Normal)
            closeBtn.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            closeBtn.addTarget(self, action: #selector(self.onClick(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            myToolBar.addSubview(closeBtn)
            
            // age用のpickerView
            let fontPickerView = UIPickerView()
            fontPickerView.showsSelectionIndicator = true
            fontPickerView.delegate = self
            fontPickerView.dataSource = self
            font!.inputView = fontPickerView
            font.inputAccessoryView = myToolBar
            fontPickerView.tag = 3
            
            cell.addSubview(font)
            
        } else if indexPath.row == 3{
            
            
            fontSize = UITextField(frame: CGRectMake(200,cell.frame.height-3.0,110,25))
            fontSize.text = ""
            fontSize.delegate = self
            fontSize.borderStyle = UITextBorderStyle.RoundedRect
            fontSize.layer.position = CGPoint(x:cell.bounds.width-2,y:cell.frame.height/2);
            
            //ToolBar作成。ニョキ担当
            myToolBar = UIToolbar(frame: CGRectMake(0, self.view.frame.size.height/6, self.view.frame.size.width, 40.0))
            myToolBar.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-20.0)
            myToolBar.backgroundColor = UIColor.whiteColor()
            myToolBar.barStyle = UIBarStyle.Black
            myToolBar.tintColor = UIColor.whiteColor()
            
            //ToolBarを閉じるボタンを追加
            let closeBtn  = UIButton(type: UIButtonType.Custom)
            closeBtn.frame = CGRectMake(self.view.frame.width/10*9,0, 50, 30)
            closeBtn.layer.position = CGPoint(x: self.view.frame.width/10*9, y:myToolBar.frame.height/2)
            closeBtn.setTitle("ok", forState: UIControlState.Normal)
            closeBtn.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            closeBtn.addTarget(self, action: #selector(self.onClick(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            myToolBar.addSubview(closeBtn)
            
            // age用のpickerView
            let fontSizePickerView = UIPickerView()
            fontSizePickerView.showsSelectionIndicator = true
            fontSizePickerView.delegate = self
            fontSizePickerView.dataSource = self
            fontSize!.inputView = fontSizePickerView
            fontSize.inputAccessoryView = myToolBar
            fontSizePickerView.tag = 4
            
            cell.addSubview(fontSize)
            
        } else if indexPath.row == 4{
    
            
            backGround = UITextField(frame: CGRectMake(200,cell.frame.height-3.0,110,25))
            backGround.text = ""
            backGround.delegate = self
            backGround.borderStyle = UITextBorderStyle.RoundedRect
            backGround.layer.position = CGPoint(x:cell.bounds.width-2,y:cell.frame.height/2);
            
            //ToolBar作成。ニョキ担当
            myToolBar = UIToolbar(frame: CGRectMake(0, self.view.frame.size.height/6, self.view.frame.size.width, 40.0))
            myToolBar.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-20.0)
            myToolBar.backgroundColor = UIColor.whiteColor()
            myToolBar.barStyle = UIBarStyle.Black
            myToolBar.tintColor = UIColor.whiteColor()
            
            //ToolBarを閉じるボタンを追加
            let closeBtn  = UIButton(type: UIButtonType.Custom)
            closeBtn.frame = CGRectMake(self.view.frame.width/10*9,0, 50, 30)
            closeBtn.layer.position = CGPoint(x: self.view.frame.width/10*9, y:myToolBar.frame.height/2)
            closeBtn.setTitle("ok", forState: UIControlState.Normal)
            closeBtn.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            closeBtn.addTarget(self, action: #selector(self.onClick(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            myToolBar.addSubview(closeBtn)
            
            // age用のpickerView
            let backGroundPickerView = UIPickerView()
            backGroundPickerView.showsSelectionIndicator = true
            backGroundPickerView.delegate = self
            backGroundPickerView.dataSource = self
            backGround!.inputView = backGroundPickerView
            backGround.inputAccessoryView = myToolBar
            backGroundPickerView.tag = 5
            
            cell.addSubview(backGround)
            
        }
    
        return cell
    }

    //表示列
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }

    //表示個数
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            return self.bubbleColorArr.count
        } else if pickerView.tag == 2{
            return self.tintColorArr.count
        } else if pickerView.tag == 3 {
            return self.fontArr.count
        } else if pickerView.tag == 4 {
            return self.fontSizeArr.count
        } else  {
            return self.backGroundArr.count
        }
    }
    
    //表示内容
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 {
            
            return self.bubbleColorArr[row] as? String
            
        } else if pickerView.tag == 2{
            
            return self.tintColorArr[row] as? String
            
        } else if pickerView.tag == 3{
            
            return self.fontArr[row] as? String
            
        } else if pickerView.tag == 4{
            
            return self.fontSizeArr[row] as? String
            
        } else {
            
            return self.backGroundArr[row] as? String
            
        }
    }
    
    //選択時
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            if pickerView.tag == 1 {
                // 吹き出しの色
                if row == 0 {
                    // pink
                    let color:String = "040040040"
                    NSUserDefaults.standardUserDefaults().setObject(color, forKey: "bubbleColor")
                }
                if row == 1 {
                    // pink
                    let color:String = "255000116"
                    NSUserDefaults.standardUserDefaults().setObject(color, forKey: "bubbleColor")
                }
                if row == 2 {
                    // pink
                    let color:String = "255255255"
                    NSUserDefaults.standardUserDefaults().setObject(color, forKey: "bubbleColor")
                }
                if row == 3 {
                    // pink
                    let color:String = "144255076"
                    NSUserDefaults.standardUserDefaults().setObject(color, forKey: "bubbleColor")
                }
                if row == 4 {
                    // pink
                    let color:String = "144255076"
                    NSUserDefaults.standardUserDefaults().setObject(color, forKey: "bubbleColor")
                }
                if row == 5 {
                    // pink
                    let color:String = "251218247"
                    NSUserDefaults.standardUserDefaults().setObject(color, forKey: "bubbleColor")
                }
                self.bubbleColor.text = self.bubbleColorArr[row] as? String
            } else if pickerView.tag == 2{
                // テキストカラー
                if row == 0 {
                    // green
                    let color:String = "000173255"
                    NSUserDefaults.standardUserDefaults().setObject(color, forKey: "tintColor")
                }
                if row == 1 {
                    // green
                    let color:String = "144255076"
                    NSUserDefaults.standardUserDefaults().setObject(color, forKey: "tintColor")
                }
                if row == 2 {
                    // green
                    let color:String = "255000116"
                    NSUserDefaults.standardUserDefaults().setObject(color, forKey: "tintColor")
                }
                if row == 3 {
                    // pink
                    let color:String = "144255076"
                    NSUserDefaults.standardUserDefaults().setObject(color, forKey: "tintColor")
                }
                if row == 4 {
                    // pink
                    let color:String = "251218247"
                    NSUserDefaults.standardUserDefaults().setObject(color, forKey: "tintColor")
                }
                self.tintColor.text = self.tintColorArr[row] as? String
            } else if pickerView.tag == 3{
                if row == 0 {
                    let color:String = "Verdana"
                    NSUserDefaults.standardUserDefaults().setObject(color, forKey: "font")
                }
                if row == 1 {
                    let color:String = "JK-Gothic-M"
                    NSUserDefaults.standardUserDefaults().setObject(color, forKey: "font")
                }
                if row == 2 {
                    let color:String = "OmePlus-P-Gothic-light"
                    NSUserDefaults.standardUserDefaults().setObject(color, forKey: "font")
                }
                if row == 3 {
                    let color:String = "F910-Shin-comic-tai"
                    NSUserDefaults.standardUserDefaults().setObject(color, forKey: "font")
                }
                self.font.text = self.fontArr[row] as? String
                
            } else if pickerView.tag == 4{
                
                if row == 0 {
                    NSUserDefaults.standardUserDefaults().setObject("12", forKey: "fontSize")
                }
                if row == 1 {
                    NSUserDefaults.standardUserDefaults().setObject("24", forKey: "fontSize")
                }
                if row == 2 {
                    NSUserDefaults.standardUserDefaults().setObject("50", forKey: "fontSize")
                }
                self.fontSize.text = self.fontSizeArr[row] as? String
                
            } else {
                
                if row == 0 {
                    // pink
                    let color:String = "040040040"
                    NSUserDefaults.standardUserDefaults().setObject(color, forKey: "backGround")
                }
                if row == 1 {
                    // pink
                    let color:String = "255000116"
                    NSUserDefaults.standardUserDefaults().setObject(color, forKey: "backGround")
                }
                if row == 2 {
                    // pink
                    let color:String = "255255255"
                    NSUserDefaults.standardUserDefaults().setObject(color, forKey: "backGround")
                }
                if row == 3 {
                    // pink
                    let color:String = "144255076"
                    NSUserDefaults.standardUserDefaults().setObject(color, forKey: "backGround")
                }
                if row == 4 {
                    // pink
                    let color:String = "144255076"
                    NSUserDefaults.standardUserDefaults().setObject(color, forKey: "backGround")
                }
                if row == 5 {
                    // pink
                    let color:String = "251218247"
                    NSUserDefaults.standardUserDefaults().setObject(color, forKey: "backGround")
                }
                self.backGround.text = self.backGroundArr[row] as? String
            }
    }
    
    func hoge()  {

    }
    
    //閉じる
    func onClick(sender: UIBarButtonItem) {
        self.view.endEditing(true)
    }

    /*
    // MARK: - Navigation
     
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
