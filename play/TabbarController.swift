//
//  TabbarControllerViewController.swift
//  play
//
//  Created by munetomoissei on 2016/06/28.
//  Copyright © 2016年 munetomoissei. All rights reserved.
//

import UIKit
import Foundation
import UIColor_MLPFlatColors

class TabbarController: UITabBarController,UITabBarControllerDelegate {

    @IBOutlet weak var bar: UITabBar!
    
    let app:AppDelegate =
        (UIApplication.sharedApplication().delegate as! AppDelegate)

    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        let font:UIFont! = UIFont(name:"HelveticaNeue-Bold",size:10)
        let selectedAttributes:[String:AnyObject]! = [NSFontAttributeName : font, NSForegroundColorAttributeName : UIColor.flatBlackColor()]
        
        UITabBarItem.appearance().setTitleTextAttributes(selectedAttributes, forState: UIControlState.Selected)
        
        UITabBar.appearance().tintColor = UIColor.flatBlackColor()

    }
    
    override func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        tabBar.sizeToFit()
        if item.tag == 1 {
            self.app.selectedTab = "Main"
        }else if item.tag == 2 {
            self.app.selectedTab = "Setting"
        }
    }
    
    func reload(){
        self.loadView()
        self.viewDidLoad()
    }
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        if viewController.tabBarItem.tag == 1 {
            let top1ViewController = Top1ViewController()
            top1ViewController.reload()
            viewController.tabBarItem.badgeValue = nil
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

