//
//  Under1ViewController.swift
//  play
//
//  Created by munetomoissei on 2016/06/15.
//  Copyright © 2016年 munetomoissei. All rights reserved.
//

import UIKit
import QuartzCore
import FontAwesome_swift

class UserViewController: UIViewController {
    // AppDelegateクラスのメンバー変数を参照する
    let app:AppDelegate =
        (UIApplication.sharedApplication().delegate as! AppDelegate)
    // width
    let w = UIScreen.mainScreen().bounds.size.width

    @IBOutlet weak var tabitem: UITabBarItem!
    
    @IBOutlet weak var userimage: UIImageView!
    
    @IBOutlet weak var username: UILabel!
    
    @IBOutlet weak var logo: UIImageView!
    
    @IBOutlet weak var header: UILabel!
    
    @IBOutlet weak var settingView: UITableView!
    
    @IBOutlet weak var logoTopMargin: NSLayoutConstraint!
    @IBOutlet weak var headerW: NSLayoutConstraint!
    @IBOutlet weak var headerH: NSLayoutConstraint!
    @IBOutlet weak var logoH: NSLayoutConstraint!
    @IBOutlet weak var logoW: NSLayoutConstraint!
    @IBOutlet weak var tWifth: NSLayoutConstraint!
    
    @IBOutlet weak var imageW: NSLayoutConstraint!
    @IBOutlet weak var imageH: NSLayoutConstraint!
    @IBOutlet weak var nameH: NSLayoutConstraint!
    
    // 下部のコントローラー
    var setting: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // autolayautoを設定する
        headerW.constant = self.view.frame.width
        headerH.constant = self.app.headerH
        logoH.constant = self.app.logoH
        logoW.constant = self.app.logoW
        logoTopMargin.constant = self.app.logoTopMargin
        tWifth.constant = self.view.frame.width
        
        let logoImg = UIImage(named: "img/destiny-logo.png")
        // 画像をUIImageViewに設定する.
        self.logo.image = logoImg
        header.backgroundColor = self.app.headerColor
        
        // tabitemの編集
        let image = UIImage.fontAwesomeIconWithName(.User, textColor:UIColor.whiteColor(),size: CGSizeMake(50, 50))
        self.tabBarItem.image = image
        
        // ユーザー画像の設定
        // logo画像を設定
        // 表示する画像を設定する.
        userimage.image = self.app.userImage
        userimage.layer.cornerRadius = 30;
        userimage.clipsToBounds = true;
        
        self.view.backgroundColor = UIColor.flatWhiteColor()
 
        // username表示
        username.text = self.app.name;
        username.textColor = self.app.textColor
        username.textAlignment = NSTextAlignment.Center
        username.font = UIFont(name:"OmePlus-P-Gothic-light",size: 25)
        username.sizeToFit()
        
        // 設定ビューの表示
        setting = storyboard!.instantiateViewControllerWithIdentifier("setting")
        setting.view.frame = settingView.bounds
        
        addChildViewController(setting)
        settingView.separatorColor = UIColor.clearColor()
        settingView.addSubview(setting.view)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
