//
//  Under3ViewController.swift
//  play
//
//  Created by munetomoissei on 2016/06/21.
//  Copyright © 2016年 munetomoissei. All rights reserved.
//

import UIKit
import NCMB
import Firebase

class SearchViewController: UIViewController, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate {
    // 検索バー
    @IBOutlet weak var searchField: UISearchBar!
    @IBOutlet weak var searchResults: UITableView!
    @IBOutlet weak var height: NSLayoutConstraint!
    private var item: [Dictionary<String,String>]!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var headerW: NSLayoutConstraint!
    @IBOutlet weak var headerH: NSLayoutConstraint!
    @IBOutlet weak var logoH: NSLayoutConstraint!
    @IBOutlet weak var logoW: NSLayoutConstraint!
    @IBOutlet weak var logoTopMargin: NSLayoutConstraint!
    @IBOutlet weak var tableW: NSLayoutConstraint!
    @IBOutlet weak var searchCellW: NSLayoutConstraint!
    
    @IBOutlet weak var searchBarTopMArgin: NSLayoutConstraint!
    // AppDelegateクラスのメンバー変数を参照する
    let app:AppDelegate =
        (UIApplication.sharedApplication().delegate as! AppDelegate)
    var refRoomApply: Firebase = Firebase(url: "https://playroom.firebaseio.com/")
    var refUser: Firebase = Firebase(url: "https://playuser.firebaseio.com/")
    
    // 関係ないところを触るとギーボードを閉じる
    @IBAction func tapScreen(sender
        : AnyObject) {
        self.view.endEditing(true)
        self.item = [["toUserId": "", "userName": ""]]
    }
    
    @IBOutlet weak var tabitem: UITabBarItem!
    private var text: String!
    private var row: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // autolayautoを設定する
        headerW.constant = self.view.frame.width
        headerH.constant = self.app.headerH
        logoH.constant = self.app.logoH
        logoW.constant = self.app.logoW
        logoTopMargin.constant = self.app.logoTopMargin
        tableW.constant = self.view.frame.width
        searchCellW.constant = self.view.frame.width
        searchBarTopMArgin.constant = 0
        let logoImg = UIImage(named: "img/destiny-logo.png")
        //        // 画像をUIImageViewに設定する.
        self.logo.image = logoImg
        header.backgroundColor = self.app.headerColor
        self.view.backgroundColor = UIColor.flatWhiteColor()


        // 表示させたいリストをセット
        self.searchResults.registerClass(UITableViewCell.self, forCellReuseIdentifier: "tcell")
        self.searchResults.dataSource = self
        self.searchResults.delegate = self
//        self.searchResults.separatorColor = UIColor.clearColor()
        self.searchField.delegate = self
        searchResults.backgroundColor = UIColor.flatWhiteColor()
        
        searchResults.hidden = true
        
        item = [["toUserId": "", "userName": ""]]
        
        // tabitemの編集
        let orgImg = UIImage(named : "img/search.png")
        let resizedSize = CGSizeMake(40, 40);
        UIGraphicsBeginImageContext(resizedSize);
        orgImg?.drawInRect(CGRectMake(0, 0, resizedSize.width, resizedSize.height))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        self.tabitem.image = resizedImage
    }

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.item.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCellWithIdentifier("tcell", forIndexPath: indexPath)
        let row = indexPath.row
        self.row = row
        let toUserId = self.item[row]["toUserId"]! as String
        cell.textLabel?.textColor = UIColor(red:255.0/255,green:0.0/255,blue:116.0/255,alpha:1.0)
        cell.backgroundColor = UIColor.flatWhiteColor()

        cell.textLabel?.text = self.item[row]["userName"]
        
        // 検索結果があれば
        if self.item[0]["userName"] != "" {
            
            // 友達が１人でもいれば
            if self.app.chat != nil {
                
                var id = [String]()
                id = []
                for item in self.app.chat {
                    id.append(item["toUserId"]!)
                }
                
                // 新規の友達なら
                if !id.contains(toUserId) {
                    // 追加ボタンの生成
                    let button = UIButton()
                    //表示されるテキスト
                    button.setTitle("Add", forState: .Normal)
                    button.setTitleColor(UIColor(red:0.0/255,green:173.0/255,blue:255.0/255,alpha:1.0), forState: .Normal)
                    button.frame = CGRectMake(0, 10, 55, 28)
                    button.tag = row
                    button.layer.position = CGPoint(x: self.view.frame.width/2, y:100)
                    button.backgroundColor = UIColor.flatWhiteColor()
                    button.layer.cornerRadius = 5
                    button.layer.borderWidth = 1
                    button.layer.borderColor = UIColor(red:0.0/255,green:173.0/255,blue:255.0/255,alpha:1.0).CGColor
                    button.addTarget(self, action: #selector(self.buttonTapped(_:)), forControlEvents: .TouchUpInside)
                    cell.accessoryView = button
                    
                } else {
                    // 既存の友達なら
                    // ボタンの生成
                    let button = UIButton()
                    //表示されるテキスト
                    button.setTitle("already✔︎", forState: .Normal)
                    button.setTitleColor(UIColor(red:154.0/255,green:255/255,blue:76/255,alpha:1.0), forState: .Normal)
                    button.frame = CGRectMake(0, 10,90, 28)
                    button.layer.position = CGPoint(x: self.view.frame.width/2, y:100)
                    button.backgroundColor = UIColor.flatWhiteColor()
                    button.layer.cornerRadius = 5
                    button.layer.borderWidth = 1
                    button.layer.borderColor = UIColor(red:154.0/255,green:255/255,blue:76/255,alpha:1.0).CGColor
                    cell.accessoryView = button
                }
            } else {
                // 追加ボタンの生成
                let button = UIButton()
                //表示されるテキスト
                button.setTitle("Add", forState: .Normal)
                button.setTitleColor(UIColor(red:0.0/255,green:173.0/255,blue:255.0/255,alpha:1.0), forState: .Normal)
                button.frame = CGRectMake(0, 10, 55, 28)
                button.tag = row
                button.layer.position = CGPoint(x: self.view.frame.width/2, y:100)
                button.backgroundColor = UIColor.flatWhiteColor()
                button.layer.cornerRadius = 5
                button.layer.borderWidth = 1
                button.layer.borderColor = UIColor(red:0.0/255,green:173.0/255,blue:255.0/255,alpha:1.0).CGColor
                button.addTarget(self, action: #selector(self.buttonTapped(_:)), forControlEvents: .TouchUpInside)
                cell.accessoryView = button
            }

        } else {
            let button = UIButton()
            cell.accessoryView = button

        }
        return cell
    }
    
    func buttonTapped(sender: UIButton) -> Bool {
        
        let tag = sender.tag
        
        //roomを作成
        var saveError: NSError?
        let now = NSDate()
        let obj = NCMBObject(className: "Room")
        obj.setObject(now, forKey: "inputDate")
        obj.setObject(self.app.name, forKey: "createUser")
        obj.save(&saveError)
        
        // ルームのidを取得する
        let query = NCMBQuery(className: "Room")
        query.whereKey("createUser", equalTo: self.app.name)
        query.whereKey("inputDate", equalTo: now)
        query.findObjectsInBackgroundWithBlock { (objects: [AnyObject]!, error: NSError!) in
            let roomID  = objects[0].objectForKey("objectId") as! String
            
            // firebaseにトークの作成を通知する
            //firebaseにデータを送信、保存する
            let userId = self.item[tag]["toUserId"]! as String
            let dictionary = ["id": roomID, "toUserId": userId,"action":"invite","fromUser":self.app.name!,"senderId": self.app.userId!]
            let post1Ref = self.refRoomApply.childByAutoId()
            post1Ref.setValue(dictionary)
            
            // 友達リストに追加する
            if self.item.count > 0 {
                
                if self.app.chat != nil{
                    self.app.chat.append(["roomID": roomID, "toUser": self.item[tag]["userName"]! as String,"toUserId": self.item[tag]["toUserId"]! as String])
                } else {
                    var cDictionary:[Dictionary<String,String>] = []
                    cDictionary.append(["roomID": roomID, "toUser": self.item[tag]["userName"]! as String,"toUserId": self.item[tag]["toUserId"]! as String])
                    self.app.chat = cDictionary
                }
                NSUserDefaults.standardUserDefaults().setObject(self.app.chat, forKey: "chat")
                self.searchResults.reloadData()
            }
        }
        return true
    }
    
    // MARK: UITableViewDelegate
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        self.searchField.text = self.item[indexPath.row]["userName"]
    }
    
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
        self.item = [["toUserId": "", "userName": ""]]
        self.searchResults.hidden = false
        return true
    }
    func searchBarShouldEndEditing(searchBar: UISearchBar) -> Bool {
        self.searchResults.hidden = true
        self.item = [["toUserId": "", "userName": ""]]
        self.searchResults.reloadData()
        return true
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.text = searchText
        
        print(searchText)
        print(2)
        var t = 0
        var count = 0
        
        if searchText == "" {
            self.item = [["toUserId": "", "userName": ""]]
            self.searchResults.reloadData()
        } else {
            self.refUser.queryOrderedByChild("userName").queryStartingAtValue(searchText).queryLimitedToLast(10).observeEventType(FEventType.ChildAdded, withBlock: { (snapshot) in

                // firebaseの２重呼び出し防止(なぜかfirebaseが２回queryを投げてその合計を取得結果としているため)
                if count == 0{
                    count = 1
                    print(snapshot.key)
                    let id = snapshot.key
                    self.item = []
                    let a = t.description
                    // 自分でなければ検索結果に追加
                    if id != self.app.userId {
                        self.item.append(["toUserId": snapshot.key, "userName": snapshot.value["userName"] as! String,"row":a])
                        t += 1
                    }
                } else {
                    print(snapshot.key)

                    if(snapshot.value.count > 0) {
                        // 該当する友達がいる場合
                        let a = t.description
                        let id = snapshot.key
                        // 自分でなければ検索結果に追加
                        if id != self.app.userId {
                            self.item.append(["toUserId": snapshot.key, "userName": snapshot.value["userName"] as! String,"row":a])
                            t += 1
                        }
                        print(self.item)
                        self.searchResults.reloadData()
                        
                    } else {
                        // 該当する友達がいない場合
                        self.searchResults.reloadData()
                    }
                }
            })
        }
    
    func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
}
