//
//  AddChatViewController.swift
//  play
//
//  Created by munetomoissei on 2016/07/14.
//  Copyright © 2016年 munetomoissei. All rights reserved.
//

import UIKit
import NCMB
import Firebase

class AddChatViewController: UIViewController {
    
    @IBOutlet weak var shortCutText: UITextView!
    
    @IBOutlet weak var completeButton: UIButton!
    
    @IBOutlet weak var logo: UIImageView!
    
    @IBOutlet weak var header: UILabel!
    
    let app:AppDelegate =
        (UIApplication.sharedApplication().delegate as! AppDelegate)
    var refRoomApply: Firebase = Firebase(url: "https://playroom.firebaseio.com/")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let logoImg = UIImage(named: "img/destiny-logo.png")
        self.logo.image = logoImg
        header.backgroundColor = UIColor(red:5.0/255,green:5.0/255,blue:5.0/255,alpha:1.0)
        
        // Do any additional setup after loading the view.
    }
    
    // okボタンが押下されたらチャットを登録する
    @IBAction func tapButton(sender: AnyObject) {
        if (self.shortCutText.text.characters.count > 0){
            //roomを作成
            var saveError: NSError?
            let now = NSDate()
            let obj = NCMBObject(className: "Room")
            obj.setObject(now, forKey: "inputDate")
            obj.setObject(self.app.name, forKey: "createUser")
            obj.save(&saveError)
            
            // ルームのidを取得する
            let query = NCMBQuery(className: "Room")
            query.whereKey("createUser", equalTo: self.app.name)
            query.whereKey("inputDate", equalTo: now)
            query.findObjectsInBackgroundWithBlock { (objects: [AnyObject]!, error: NSError!) in
                let roomID  = objects[0].objectForKey("objectId") as! String
                self.app.topRoom.append(roomID)
                self.app.topRoomName.append(self.shortCutText.text)
                
                // firebaseにトークの作成を通知する
                //firebaseにデータを送信、保存する
                let dictionary = ["id": roomID, "toUser": "","action":"invite"]
                let post1Ref = self.refRoomApply.childByAutoId()
                post1Ref.setValue(dictionary)
                
                if NSUserDefaults.standardUserDefaults().objectForKey("chat") != nil {
                    var chat :[String] = NSUserDefaults.standardUserDefaults().objectForKey("chat") as! Array
                    chat.append(self.shortCutText.text)
                    NSUserDefaults.standardUserDefaults().setObject(chat, forKey: "chat")
                } else {
                    var chat = [String]()
                    chat.append(self.shortCutText.text)
                    NSUserDefaults.standardUserDefaults().setObject(chat, forKey: "chat")
                }
                
                
                if NSUserDefaults.standardUserDefaults().objectForKey("chatAddress") != nil {
                    var chatAddress :[String] = NSUserDefaults.standardUserDefaults().objectForKey("chatAddress") as! Array
                    chatAddress.append(roomID)
                    NSUserDefaults.standardUserDefaults().setObject(chatAddress, forKey: "chatAddress")
                } else {
                    var chatAddress = [String]()
                    chatAddress.append(roomID)
                    NSUserDefaults.standardUserDefaults().setObject(chatAddress, forKey: "chatAddress")
                }
                
                NSUserDefaults.standardUserDefaults().setObject(self.app.topRoom, forKey: "topRoom")
                NSUserDefaults.standardUserDefaults().setObject(self.app.topRoomName, forKey: "topRoomName")
                // メイン画面に戻る
                let mainViewController: TabbarController = self.storyboard?.instantiateViewControllerWithIdentifier("tab") as! TabbarController
                self.presentViewController( mainViewController, animated: false, completion: nil)
            }
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
