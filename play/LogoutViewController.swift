//
//  Under2ViewController.swift
//  play
//
//  Created by munetomoissei on 2016/06/19.
//  Copyright © 2016年 munetomoissei. All rights reserved.
//

import UIKit
import Firebase

class LogoutViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    // テーブルビュー
    @IBOutlet weak var settingTable: UITableView!
    // セルに表示するテキスト
    let contents = [ "アカウント削除"]
    // AppDelegateクラスのメンバー変数を参照する
    let app:AppDelegate =
        (UIApplication.sharedApplication().delegate as! AppDelegate)
    var refUser: Firebase = Firebase(url: "https://playuser.firebaseio.com/")
    var ref: Firebase = Firebase(url: "https://playchatting.firebaseio.com/")
    var refRoom: Firebase = Firebase(url: "https://playroom.firebaseio.com/")



    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingTable.delegate = self
        settingTable.dataSource = self

        self.settingTable.separatorColor = UIColor.clearColor()
        self.settingTable.layoutMargins = UIEdgeInsetsZero
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // セルの行数
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contents.count
    }
    
    
    //セルの内容を変更
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "Cell")
        cell.layoutMargins = UIEdgeInsetsZero

        
        self.settingTable.backgroundColor = UIColor.flatWhiteColor()
        cell.backgroundColor = UIColor.flatWhiteColor()
        cell.textLabel?.text = contents[indexPath.row]
        cell.textLabel?.textColor = self.app.textColor
        cell.textLabel?.font = UIFont(name:"OmePlus-P-Gothic-light",size: 14)
        
        // 選択された背景色を設定
        let cellSelectedBgView = UIView()
        cellSelectedBgView.backgroundColor = UIColor(red: 50.0/255.0, green: 50.0/255.0, blue: 50.0/255.0, alpha: 1.0)
        cell.selectedBackgroundView = cellSelectedBgView
        
        let line =  UIView(frame : CGRectMake(0.0, cell.frame.height-1.0, settingTable.frame.size.width, 1.0))
        line.backgroundColor = UIColor(red: 110.0/255.0, green: 110.0/255.0, blue: 110.0/255.0, alpha: 1.0)
        cell.addSubview(line)
        
        return cell
    }
    
    // Cell が選択された場合
    func tableView(table: UITableView, didSelectRowAtIndexPath indexPath:NSIndexPath) {
        if indexPath.row == 0 {
            self.deleteAccount()
        }
    }
    
    func deleteAccount() {
        let alert: UIAlertController = UIAlertController(title: "ユーザー削除", message: "アカウント情報をすべて削除します。本当によろしいですか？", preferredStyle:  UIAlertControllerStyle.Alert)
        
        let defaultAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:{
            
            (action: UIAlertAction!) -> Void in
            
            
            // ユーザー削除
            self.refUser.queryOrderedByKey().queryEqualToValue(self.app.userId).observeEventType(FEventType.ChildAdded, withBlock: { (snapshot) in
                    snapshot.ref.removeValue()
            })
            
            // チャット申請情報削除
            self.refRoom.queryOrderedByChild("senderId").queryEqualToValue(self.app.userId).observeEventType(FEventType.ChildAdded, withBlock: { (snapshot) in
                snapshot.ref.removeValue()
            })
            
 
            
            // 端末データ削除
            let appDomain = NSBundle.mainBundle().bundleIdentifier
            NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain!)

            
            let controller: WelcomeViewController = self.storyboard?.instantiateViewControllerWithIdentifier("welcome") as! WelcomeViewController
            self.presentViewController( controller, animated: false, completion: nil)
        })
        // キャンセルボタン
        let cancelAction: UIAlertAction = UIAlertAction(title: "キャンセル", style: UIAlertActionStyle.Cancel, handler:{
            // ボタンが押された時の処理を書く（クロージャ実装）
            (action: UIAlertAction!) -> Void in
        })
        
        alert.addAction(cancelAction)
        alert.addAction(defaultAction)
        
        presentViewController(alert, animated: true, completion: nil)
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
