///
//  AppDelegate.swift
//  play
//
//  Created by munetomoissei on 2016/05/04.
//  Copyright © 2016年 munetomoissei. All rights reserved.
//

import UIKit
import NCMB
import AVFoundation
import FBSDKCoreKit
import UIColor_MLPFlatColors

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,AVAudioPlayerDelegate {

    var window: UIWindow?
    
    // この変数を他のクラスからアクセスする
    //　データ受け渡しグローバルフィールド。空要素（nil）を追加する
    var globalStrings01:String? = nil
    var name:String?
    var password:String?
    var room: NSArray?
    var topRoom: [String] = []
    var topRoomName: [String] = []
    var underRoom: [String] = []
    var selectedRoom: Int = 0
    var top1tableView: UITableView!
    var top2tableView: UITableView!
    var top3tableView: UITableView!
    var top4tableView: UITableView!
    var top5tableView: UITableView!
    var top1tableData: [Dictionary<String,AnyObject>]!
    var top2tableData: [Dictionary<String,AnyObject>]!
    var top3tableData: [Dictionary<String,AnyObject>]!
    var top4tableData: [Dictionary<String,AnyObject>]!
    var top5tableData: [Dictionary<String,AnyObject>]!
    var myShortCut:[String]!
    var shortcutPath:Int?
    var dTapPlayer = AVAudioPlayer ()
    var longPressPlayer = AVAudioPlayer ()
    var chat: [Dictionary<String,String>]!
    private var myTabBarController: UITabBarController!
    var selectedTab:String = "Main"
    var tabBarHeight:CGFloat = CGFloat(49)
    let whiteColor = UIColor(red:240.0/255,green:240.0/255,blue:240.0/255,alpha:1.0)
    let blackColor = UIColor.flatWhiteColor()
    let blueColor = UIColor(red:80.0/255,green:127.0/255,blue:212.0/255,alpha:1.0)
    let lightBluekColor = UIColor(red:118.0/255,green:185.0/255,blue:247.0/255,alpha:1.0)
    let borderColor = UIColor(red: 40.0/255.0, green: 40.0/255.0, blue: 40.0/255.0, alpha: 1.0)
    let headerColor = UIColor.randomFlatColor()
    let textColor = UIColor.randomFlatColor()
    var userImage: UIImage!
    var headerH:CGFloat!
    var logoW:CGFloat!
    var logoH:CGFloat!
    var logoTopMargin:CGFloat!
    var inviteId:String!
    var userId:String!


    // NCMBアプリケーションキー
    let applicationkey = "ae828768cbd97dc722e0fe0dce876fe263f3f3c2ce3abf9a0c671ff576c4777d"
    let clientkey      = "706c6029889bfae57002e35a51c637463b36be0c49e3d082d9f9ad4c92599cfd"




    // 初回起動時に呼ばれる
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool { 
        // Override point for customization after application launch.
        
        
        NCMB.setApplicationKey(applicationkey, clientKey: clientkey)
        
        let storyboard:UIStoryboard =  UIStoryboard(name: "Main",bundle:nil)
        let mainViewController: TabbarController = storyboard.instantiateViewControllerWithIdentifier("tab") as! TabbarController
        let welcomeViewController: WelcomeViewController = storyboard.instantiateViewControllerWithIdentifier("welcome") as! WelcomeViewController
        window?.rootViewController = welcomeViewController

        
        
        // user情報があればログイン画面をスキップさせる
        if NSUserDefaults.standardUserDefaults().objectForKey("userName") != nil {
            
            
            window?.rootViewController = mainViewController
            self.name = NSUserDefaults.standardUserDefaults().objectForKey("userName") as? String
            
            //前回の保存内容があるかどうかを判定
            if((NSUserDefaults.standardUserDefaults().objectForKey("chat")) != nil){
                
                //objectsを配列として確定させ、前回の保存内容を格納
                let objects = NSUserDefaults.standardUserDefaults().objectForKey("chat") as? NSArray
                
                //各名前を格納するための変数を宣言
                var _:AnyObject
                chat = []
                
                //前回の保存内容が格納された配列の中身を一つずつ取り出す
                for nameString in objects!{
                    //配列に追加していく
                    chat.append(nameString as! Dictionary<String,String>)
                }
            }
            if NSUserDefaults.standardUserDefaults().objectForKey("invitedId") != nil {
                self.inviteId = NSUserDefaults.standardUserDefaults().objectForKey("invitedId") as! String
            }
            
            if NSUserDefaults.standardUserDefaults().objectForKey("userId") != nil {
                self.userId = NSUserDefaults.standardUserDefaults().objectForKey("userId") as! String
            }
            
            if NSUserDefaults.standardUserDefaults().objectForKey("topRoom") != nil {
                let topRoom:[String] = NSUserDefaults.standardUserDefaults().objectForKey("topRoom") as! Array
                self.topRoom = topRoom
            }
            
            if NSUserDefaults.standardUserDefaults().objectForKey("topRoomName") != nil {
                let topRoomName:[String] = NSUserDefaults.standardUserDefaults().objectForKey("topRoomName") as! Array
                self.topRoomName = topRoomName
            }
            
            if NSUserDefaults.standardUserDefaults().objectForKey("userImage") != nil {
                if let imageData = NSUserDefaults.standardUserDefaults().objectForKey("userImage") as? NSData, image = UIImage(data: imageData) {
                    // UserDefaultsから画像が取得出来た場合ImageViewのimageに設定
                    self.userImage = image
                }
            }
            
            // NCMBよりユーザー情報を取得する
            let query = NCMBQuery(className: "User")
            query.whereKey("userName", equalTo: self.name)
            query.findObjectsInBackgroundWithBlock { (objects: [AnyObject]!, error: NSError!) in
                if (error == nil) {
                    if(objects.count > 0) {
                        self.room = objects[0].objectForKey("room") as? NSArray
                    }
                }
            }
        }
        
        let dTapPath = NSURL(fileURLWithPath:NSBundle.mainBundle().pathForResource("sound/dTap", ofType: "mp3")!)
        self.dTapPlayer = try! AVAudioPlayer(contentsOfURL: dTapPath);
        self.dTapPlayer.prepareToPlay()
        
        let soundFilePath = NSURL(fileURLWithPath:NSBundle.mainBundle().pathForResource("sound/longPress", ofType: "mp3")!)
        self.longPressPlayer = try! AVAudioPlayer(contentsOfURL: soundFilePath);
        self.longPressPlayer.prepareToPlay()
  
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func application(application: UIApplication,openURL url: NSURL,sourceApplication: String?,annotation: AnyObject) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

