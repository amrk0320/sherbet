//
//  chatViewController.swift
//  play
//
//  Created by munetomoissei on 2016/06/26.
//  Copyright © 2016年 munetomoissei. All rights reserved.
//

import UIKit
import NCMB
import Firebase

class ChatListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var searchResults: UITableView!
    
    private var chat:[String]!
    var refRoomApply: Firebase = Firebase(url: "https://playroom.firebaseio.com/")
    // AppDelegateクラスのメンバー変数を参照する
    let app:AppDelegate =
        (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.flatWhiteColor()
        
        // 表示させたいリストをセット
        self.searchResults.registerClass(UITableViewCell.self, forCellReuseIdentifier: "fcell")
        self.searchResults.dataSource = self
        self.searchResults.delegate = self
        self.searchResults.separatorColor = UIColor.clearColor()
        
        self.chat = []
        
        if self.app.chat.count > 0 {
            for (_, name) in self.app.chat.enumerate() {
                 self.chat.append(name["toUser"]!)
            }
        }

        
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.chat.count == 0 {
            return 1
        }else{
            return chat.count
        }
    }
    
    //セルの内容を変更
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "fcell")
        cell.layoutMargins = UIEdgeInsetsZero
        
        if self.chat.count == 0 {
            cell.textLabel?.text = ""
        }else{
            cell.textLabel?.text = chat[indexPath.row]
        }
        cell.textLabel?.font = UIFont(name:"OmePlus-P-Gothic-light",size: 14)

        
        self.searchResults.backgroundColor = UIColor.flatWhiteColor()
        cell.backgroundColor = UIColor.flatWhiteColor()
        cell.textLabel?.textColor = self.app.textColor
        
        // 選択された背景色を設定
        let cellSelectedBgView = UIView()
        cellSelectedBgView.backgroundColor = UIColor(red: 50.0/255.0, green: 50.0/255.0, blue: 50.0/255.0, alpha: 1.0)
        cell.selectedBackgroundView = cellSelectedBgView
        
        let line =  UIView(frame : CGRectMake(0.0, cell.frame.height-1.0, searchResults.frame.size.width, 1.0))
        line.backgroundColor = UIColor(red: 110.0/255.0, green: 110.0/255.0, blue: 110.0/255.0, alpha: 1.0)
        cell.addSubview(line)
        
        return cell
    }
    
    func buttonTapped(searchBar: UISearchBar) -> Bool {
        
        return true
    }
    
    // MARK: UITableViewDelegate
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    // 友達が追加されたら宛先に追加する
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let toUser = self.app.chat[indexPath.row]["toUser"]
        let roomID  = self.app.chat[indexPath.row]["roomID"]
        
        // タブが５つ以下だったら
        if self.app.topRoom.count < 5 {
            self.app.topRoom.append(roomID!)
            self.app.topRoomName.append(toUser!)
        } else {
            // 5つ以上だったらトークルームを入れ替え
            // 配列を入れ替え
            var tmpTopRoom: [String] = []
            var tmpTopRoomName: [String] = []
            
            for (index, room) in self.app.topRoom.enumerate() {
                if index <= 5 && index != 0 {
                    tmpTopRoom.append(room)
                }
            }
            
            for (index, roomNm) in self.app.topRoomName.enumerate() {
                if index <= 5 && index != 0 {
                    tmpTopRoomName.append(roomNm)
                }
            }
            
            tmpTopRoom.append(roomID!)
            tmpTopRoomName.append(toUser!)
            
            self.app.topRoom = tmpTopRoom
            self.app.topRoomName = tmpTopRoomName
        }
        
        
        NSUserDefaults.standardUserDefaults().setObject(self.app.topRoom, forKey: "topRoom")
        NSUserDefaults.standardUserDefaults().setObject(self.app.topRoomName, forKey: "topRoomName")
        let mainViewController: TabbarController = self.storyboard?.instantiateViewControllerWithIdentifier("tab") as! TabbarController
        self.presentViewController( mainViewController, animated: false, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    }
