//
//  AddShortCutViewController.swift
//  play
//
//  Created by munetomoissei on 2016/07/01.
//  Copyright © 2016年 munetomoissei. All rights reserved.
//

import UIKit

class AddShortCutViewController: UIViewController {

    @IBOutlet weak var shortCutText: UITextView!
    
    @IBOutlet weak var completeButton: UIButton!
    
    @IBOutlet weak var logo: UIImageView!
    
    @IBOutlet weak var header: UILabel!
    
    @IBOutlet weak var headerW: NSLayoutConstraint!
    @IBOutlet weak var headerH: NSLayoutConstraint!
    
    @IBOutlet weak var logoTopMargin: NSLayoutConstraint!
    @IBOutlet weak var logoW: NSLayoutConstraint!
    @IBOutlet weak var logoH: NSLayoutConstraint!
    let app:AppDelegate =
        (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerW.constant = self.view.frame.width
        headerH.constant = self.app.headerH
        logoH.constant = self.app.logoH
        logoW.constant = self.app.logoW
        logoTopMargin.constant = self.app.logoTopMargin
        
        let logoImg = UIImage(named: "img/destiny-logo.png")
        self.logo.image = logoImg
        header.backgroundColor = self.app.headerColor
        
        self.view.backgroundColor = UIColor.flatWhiteColor()

        

        // Do any additional setup after loading the view.
    }

    // okボタンが押下されたらショートカットを登録する
    @IBAction func tapButton(sender: AnyObject) {
        
        if self.app.shortcutPath == 0 {
            NSUserDefaults.standardUserDefaults().setObject(self.shortCutText.text, forKey: "shortCutDTap")
        } else if self.app.shortcutPath == 1 {
            NSUserDefaults.standardUserDefaults().setObject(self.shortCutText.text, forKey: "longPress")
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
