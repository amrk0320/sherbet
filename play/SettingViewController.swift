//
//  Vc2ViewController.swift
//  play
//
//  Created by munetomoissei on 2016/06/01.
//  Copyright © 2016年 munetomoissei. All rights reserved.
//

import UIKit
import Firebase

class SettingViewController: UIViewController {
    
    // 下側pageMenu
    var pageMenu2 : CAPSPageMenu?
    // room
    var refRoom: Firebase = Firebase(url: "https://playroom.firebaseio.com/")
    // チャット相手の一覧を生成
    let app:AppDelegate =
        (UIApplication.sharedApplication().delegate as! AppDelegate)
    var roomName1 :String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // pageMenuの設定
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30.0/255.0, green: 30.0/255.0, blue: 30.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.barStyle = UIBarStyle.Black
        self.navigationController?.navigationBar.tintColor = UIColor.flatBlackColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.orangeColor()]
        
        var controllerArray : [UIViewController] = []
        
        let controller1 : UIViewController =  storyboard!.instantiateViewControllerWithIdentifier("shortcut")
        
        controllerArray.append(controller1)
        
        let controller2 : UIViewController =  storyboard!.instantiateViewControllerWithIdentifier("logout")
        controller2.title = "logout"
        
        controllerArray.append(controller2)
        
        let controller3 : UIViewController =  storyboard!.instantiateViewControllerWithIdentifier("chat")
        controller3.title = "list"
        
        controllerArray.append(controller3)
        
        let controller4 : UIViewController =  storyboard!.instantiateViewControllerWithIdentifier("chatSetting")
        controller4.title = "chat"
        
        controllerArray.append(controller4)
        
        
        // pageMenuのバーの設定
        let parameters: [CAPSPageMenuOption] = [
            .ScrollMenuBackgroundColor(self.app.blackColor),
            .ViewBackgroundColor(UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 1.0)),
            .SelectionIndicatorColor(UIColor.flatBlackColor()),
            .BottomMenuHairlineColor(UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 70.0/255.0, alpha: 1.0)),
            .MenuItemFont(UIFont(name: "HelveticaNeue", size: 13.0)!),
            .MenuHeight(50.0),
            .MenuItemWidth(80.0),
            .CenterMenuItems(false)
        ]
        
        pageMenu2 = CAPSPageMenu(viewControllers: controllerArray, frame: CGRectMake(0.0, 0.0, self.view.frame.width, self.view.frame.height), pageMenuOptions: parameters)
        
        
        self.addChildViewController(pageMenu2!)
        self.view.addSubview(pageMenu2!.view)
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
