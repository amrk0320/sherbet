//
//  ShortCutViewController.swift
//  play
//
//  Created by munetomoissei on 2016/07/01.
//  Copyright © 2016年 munetomoissei. All rights reserved.
//

import UIKit

class ShortCutViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var shortCutList: UITableView!
    
    private var shortCut:[String]!
    // AppDelegateクラスのメンバー変数を参照する
    let app:AppDelegate =
        (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    override func viewDidLoad() {
        
        self.title = "shortCut"
        
        self.view.backgroundColor = UIColor.flatWhiteColor()
        
        // 表示させたいリストをセット
        self.shortCutList.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.shortCutList.dataSource = self
        self.shortCutList.delegate = self
        self.shortCutList.separatorColor = UIColor.clearColor()
        
        self.shortCut = ["ダブルタップ","長押し"]
        
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.shortCut.count == 0 {
            return 1
        }else{
            return shortCut.count
        }
    }
    
    //セルの内容を変更
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "cell")
        cell.layoutMargins = UIEdgeInsetsZero
        
        if self.shortCut.count == 0 {
            cell.textLabel?.text = ""
        }else{
            cell.textLabel?.text = shortCut[indexPath.row]
            cell.textLabel?.font = UIFont(name:"OmePlus-P-Gothic-light",size: 14)
        }
        
        self.shortCutList.backgroundColor = UIColor.flatWhiteColor()
        cell.backgroundColor = UIColor.flatWhiteColor()
        cell.textLabel?.textColor = self.app.textColor
        
        // 選択された背景色を設定
        let cellSelectedBgView = UIView()
        cellSelectedBgView.backgroundColor = UIColor(red: 50.0/255.0, green: 50.0/255.0, blue: 50.0/255.0, alpha: 1.0)
        cell.selectedBackgroundView = cellSelectedBgView
        
        let line =  UIView(frame : CGRectMake(0.0, cell.frame.height-1.0, shortCutList.frame.size.width, 1.0))
        line.backgroundColor = UIColor(red: 110.0/255.0, green: 110.0/255.0, blue: 110.0/255.0, alpha: 1.0)
        cell.addSubview(line)
        
        return cell
    }
    
    func buttonTapped(searchBar: UISearchBar) -> Bool {
        
        return true
    }
    
    // MARK: UITableViewDelegate
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        // ショートカット追加の画面へ
        let addShortCutViewController: AddShortCutViewController = self.storyboard?.instantiateViewControllerWithIdentifier("addShortCut") as! AddShortCutViewController
        
        self.app.shortcutPath = indexPath.row
    
        // Viewの移動する.
        self.presentViewController( addShortCutViewController, animated: true, completion: nil)
        
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell,
                   forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = self.app.blackColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
